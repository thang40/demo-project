import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { checkAuthAction, selectAuthState } from '../ducks/auth.duck';
import { selectUserInfo, getUserInfoAction, selectIsUserInfoExists } from '../ducks/user.duck';
import { logoutAction } from '../ducks/login.duck';

const enhanceHeader = (HeaderComponent: any) => {
  const Wrapper = () => {
    return class extends Component<any> {
      componentWillMount() {
        const { getUserInfoAction, checkAuthAction } = this.props;
        checkAuthAction();
      }

      componentWillUpdate(nextProps: any) {
        const { isAuthenticated, isUserInfoExists } = nextProps;
        if (isAuthenticated && !isUserInfoExists) {
          getUserInfoAction();
        }
      }

      render() {
        const { isAuthenticated, userInfo, logoutAction } = this.props;
        return <HeaderComponent isAuthenticated={isAuthenticated} userInfo={userInfo} logoutAction={logoutAction} />;
      }
    };
  };

  const mapStateToProps = (state: any, ownProps: any) => {
    return {
      ...selectAuthState(state),
      userInfo: selectUserInfo(state),
      isUserInfoExists: selectIsUserInfoExists(state)
    };
  };

  return withRouter(
    connect(
      mapStateToProps,
      { checkAuthAction, logoutAction, getUserInfoAction }
    )(Wrapper())
  );
};

export default enhanceHeader;
