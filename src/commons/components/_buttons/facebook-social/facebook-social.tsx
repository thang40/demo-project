import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import styles from './facebook-social.module.scss';

const FB_API_CLIENT_ID = process.env.REACT_APP_FB_API_CLIENT_ID || '';
const facebookApi = { clientId: FB_API_CLIENT_ID };

interface LoginFormPropTypes {
  handleLoginAction: any;
  buttonTitle: 'Login with Facebook';
}

export class FacebookSocial extends Component<LoginFormPropTypes, {}> {
  props: any;

  constructor(props: any) {
    super(props);

    this.responseFacebook = this.responseFacebook.bind(this);
  }

  responseFacebook(response: any) {
    const { handleLoginAction } = this.props;
    if (response.accessToken) {
      handleLoginAction({ access_token: response.accessToken });
    }
  }

  render() {
    const { buttonTitle } = this.props;
    return (
      <FacebookLogin
        appId={facebookApi.clientId}
        autoLoad={false}
        fields="name,email,picture"
        callback={this.responseFacebook}
        cssClass={`btn block btn--icon bg--facebook type--uppercase ${styles['custom-button']}`}
        icon="fa-facebook"
        textButton={buttonTitle}
      />
    );
  }
}
