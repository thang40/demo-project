import React, { Component } from 'react';
import styles from './linkedin-social.module.scss';
import SocialButton from './SocialButton';

const LINKEDIN_API_CLIENT_ID = '81pzwamekxm609'; //process.env.REACT_APP_LINKEDIN_API_CLIENT_ID || '';
const LinkedinApi = { clientId: LINKEDIN_API_CLIENT_ID };

interface LoginFormPropTypes {
  handleLoginAction: any;
  buttonTitle: 'Login with LinkedIn';
}

export class LinkedinSocial extends Component<LoginFormPropTypes, {}> {
  props: any;

  constructor(props: any) {
    super(props);

    this.responseLinkedin = this.responseLinkedin.bind(this);
  }

  responseLinkedin(response: any) {
    const { handleLoginAction } = this.props;
    if (response._token.accessToken) {
      handleLoginAction({ access_token: response._token.accessToken });
    }
  }
  responseFailure(response: any) {
    console.log('Failed');
  }

  render() {
    const { buttonTitle } = this.props;
    return (
      <div>
        <SocialButton
          provider="linkedin"
          appId={LinkedinApi.clientId}
          onLoginSuccess={this.responseLinkedin}
          onLoginFailure={this.responseFailure}
        >
          {buttonTitle}
        </SocialButton>
      </div>
    );
  }
}
