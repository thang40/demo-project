import React from 'react';
import styles from './menu-icon-button.module.scss';

interface IconButtonPropTypes {
  iconClass: string;
  buttonName: string;
  className?: string;
}

export const IconButton = (props: IconButtonPropTypes) => {
  const { iconClass, buttonName, className } = props;
  return (
    <div className={`row align-items-center ${styles['contain']} ${className}`}>
      <i className={`${iconClass} ${styles['size-icon']}`} />
      <span className={styles['size-text']}>{buttonName}</span>
    </div>
  );
};
