import React from 'react';
import styles from './toggle-button.module.scss';

interface ToggleButtonPropTypes {
  handleToggle?: any;
  isChecked?: boolean;
}

export const ToggleButton = (props: ToggleButtonPropTypes) => (
  <label className={styles['input-toggle']}>
    <input type="checkbox" checked={props.isChecked} onChange={props.handleToggle} /> <span />
  </label>
);
