import React, { Component } from 'react';
import styles from './education-form.module.scss';
import { FormError } from '../../form-error/form-error';
import { LoadingButton } from '../../_buttons';
import { MonthYearRange } from '../../month-year-range/month-year-range';
import { ImagePreviewUpload } from '../../image-preview-upload/image-preview-upload';

interface EducationFormPropTypes {
  form: any;
  onSubmit: any;
  isLoading: boolean;
  title: string;
  submitBtnTitle: string;
}

class EducationForm extends Component<EducationFormPropTypes, {}> {
  image = {
    name: 'logoUploader',
    label: 'Logo'
  };

  schoolName = {
    name: 'school_name',
    label: 'School*',
    placeholder: 'School name',
    options: {
      initialValue: '',
      rules: [{ required: true, message: "School's name is required" }]
    }
  };

  degreeName = {
    name: 'degree_name',
    label: 'Degree*',
    placeholder: 'Degree name',
    options: {
      initialValue: '',
      rules: [{ required: true, message: 'Degree is required' }]
    }
  };

  location = {
    name: 'location',
    label: 'Location*',
    placeholder: 'Location',
    options: {
      initialValue: '',
      rules: [{ required: true, message: 'Location is required' }]
    }
  };

  timeline = {
    name: 'timeline',
    placeholder: 'Location',
    options: {
      initialValue: {
        fromMonth: 1,
        toMonth: 1,
        fromYear: 2018,
        toYear: 2018,
        currentlyWorkHere: false
      },
      rules: [
        {
          validator: (rule: any, value: any, callback: any) => {
            const { fromMonth, toMonth, fromYear, toYear, currentlyWorkHere } = value;
            if (fromYear < toYear || currentlyWorkHere) {
              callback();
              return;
            }
            if (fromYear > toYear) {
              callback('Your end date can’t be earlier than your start date.');
            }
            if (fromYear === toYear && fromMonth > toMonth) {
              callback('Your end date can’t be earlier than your start date.');
            }

            callback();
            return;
          }
        }
      ]
    }
  };

  gpa = {
    name: 'gpa',
    label: 'GPA',
    placeholder: 'GPA',
    options: {
      initialValue: '',
      rules: [
        {
          validator: (rule: any, value: any, callback: any) => {
            const intValue = Number(value.toString().trim());
            if (value && isNaN(value)) {
              callback('GPA must be an integer');
            }
            if ((value && intValue < 1) || intValue > 10) {
              callback('GPA must be between 1 and 10');
            }
            if (!value) {
              callback('GPA is required');
            }

            callback();
            return;
          }
        }
      ]
    }
  };

  description = {
    name: 'description',
    label: 'Description',
    placeholder: 'Description',
    options: {
      initialValue: '',
      rules: []
    }
  };

  handleSubmit = (e: any) => {
    e.preventDefault();
    const { form, onSubmit, isLoading } = this.props;
    form.validateFields((error: any, value: any) => {
      if (!error) {
        if (value[this.image.name]) {
          const { uploadFile } = value[this.image.name];
          !isLoading && onSubmit(value, uploadFile);
        } else {
          !isLoading && onSubmit(value);
        }
      }
    });
  };

  renderErrorSection = (name: string) => {
    const { getFieldError } = this.props.form;
    const errors = getFieldError(name);
    return errors
      ? errors.map((err: any, index: any) => {
          return <FormError key={index} text={err} />;
        })
      : null;
  };

  render() {
    const { getFieldProps } = this.props.form;
    const { isLoading, title, submitBtnTitle } = this.props;
    return (
      <div className="row">
        <div className="col-md-12">
          <form className={styles['form']} onSubmit={this.handleSubmit}>
            <div className="container">
              <h2 className="text-center">{title}</h2>
              <div className="row">
                <div className="col-12">
                  <label>{this.image.label}</label>
                  <ImagePreviewUpload {...getFieldProps(this.image.name)} />
                </div>
                {/* school name */}
                <div className="col-12 text-left">
                  <label>{this.schoolName.label}</label>
                  <input
                    type="text"
                    placeholder={this.schoolName.placeholder}
                    autoFocus
                    {...getFieldProps(this.schoolName.name, this.schoolName.options)}
                  />
                  <div>{this.renderErrorSection(this.schoolName.name)}</div>
                </div>
                {/* degree name */}
                <div className="col-md-12 text-left">
                  <label>{this.degreeName.label}</label>
                  <input
                    type="text"
                    placeholder={this.degreeName.placeholder}
                    {...getFieldProps(this.degreeName.name, this.degreeName.options)}
                  />
                  <div>{this.renderErrorSection(this.degreeName.name)}</div>
                </div>
                {/* month year from-to */}
                <div className="col-md-12 text-left">
                  <MonthYearRange
                    checkboxLabel="Currently study here"
                    {...getFieldProps(this.timeline.name, this.timeline.options)}
                  />
                  <div>{this.renderErrorSection(this.timeline.name)}</div>
                </div>
                {/* gpa */}
                <div className="col-md-6 text-left">
                  <label>{this.gpa.label}</label>
                  <input
                    type="text"
                    placeholder={this.gpa.placeholder}
                    {...getFieldProps(this.gpa.name, this.gpa.options)}
                  />
                  <div>{this.renderErrorSection(this.gpa.name)}</div>
                </div>
                {/* description */}
                <div className="col-md-12 text-left">
                  <label>{this.description.label}</label>
                  <textarea
                    rows={3}
                    placeholder={this.description.placeholder}
                    {...getFieldProps(this.description.name, this.description.options)}
                  />
                  <div>{this.renderErrorSection(this.description.name)}</div>
                </div>

                <div className={`col-md-12 ${styles['button-wrapper']}`}>
                  <LoadingButton isLoading={isLoading} type="submit" text={submitBtnTitle} />
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default EducationForm;
