import React, { Component } from 'react';
import { FormError } from '../../form-error/form-error';
import { MonthYearRange } from '../../month-year-range/month-year-range';
import styles from './experience-form.module.scss';
import { LoadingButton } from '../../_buttons';
import { ImagePreviewUpload } from '../../image-preview-upload/image-preview-upload';
import { enhanceLocationSelect } from '../../../../HOCs';
import { LocationAutoComplete } from '../../tag-input/location-autocomplete';

const EnhancedLocationSelect = enhanceLocationSelect();

interface ExperienceFormPropTypes {
  form: any;
  onSubmit: any;
  isLoading: boolean;
  title: string;
  submitBtnTitle: string;
  onUploadLogo?: any;
  data?: any;
}

class ExperienceForm extends Component<ExperienceFormPropTypes, {}> {
  image = {
    name: 'logoUploader',
    label: 'Logo',
    options: {
      initialValue: {
        image_url: null,
        uploadFile: null
      }
    }
  };

  state = {
    city: '',
    country: '',
    state: '',
    cityName: '',
    countryName: '',
    stateName: ''
  }
  title = {
    name: 'title',
    label: 'Title*',
    placeholder: 'Title',
    options: {
      initialValue: '',
      rules: [{ required: true, message: 'Title is required' }]
    }
  };
  // places = {
  //   name: 'places',
  //   options: {
  //     initialValue: {
  //       country: -1,
  //       state: -1,
  //       city: -1
  //     },
  //     rules: [
  //       {
  //         validator: (rule: any, value: any, callback: any) => {
  //           if (value && value.selectedCountryId === -1) {
  //             callback('Country is required');
  //           }
  //           if (value && value.selectedStateId === -1) {
  //             callback('State is required');
  //           }
  //           if (value && value.selectedCityId === -1) {
  //             callback('City is required');
  //           }
  //           callback();
  //           return;
  //         }
  //       }
  //     ]
  //   }
  // };

  address = {
    name: 'address',
    label: 'Address*',
    placeholder: 'Address',
    options: {
      initialValue: '',
      rules: [
        {
          required: true,
          message: 'Address is required',
          transform: (value: string) => value.trim()
        }
      ]
    }
  };

  company = {
    name: 'company',
    label: 'Company*',
    placeholder: 'Company',
    options: {
      initialValue: '',
      rules: [{ required: true, message: 'Company is required' }]
    }
  };

  timeline = {
    name: 'timeline',
    options: {
      initialValue: {
        fromMonth: 1,
        toMonth: 1,
        fromYear: new Date().getFullYear(),
        toYear: new Date().getFullYear(),
        currentlyWorkHere: false
      },
      rules: [
        {
          validator: (rule: any, value: any, callback: any) => {
            const { fromMonth, toMonth, fromYear, toYear, currentlyWorkHere } = value;
            if (fromYear < toYear || currentlyWorkHere) {
              callback();
              return;
            }
            if (fromYear > toYear) {
              callback('Your end date can’t be earlier than your start date.');
            }
            if (fromYear === toYear && fromMonth > toMonth) {
              callback('Your end date can’t be earlier than your start date.');
            }

            callback();
            return;
          }
        }
      ]
    }
  };

  description = {
    name: 'description',
    label: 'Description',
    placeholder: 'Description',
    options: {
      initialValue: '',
      rules: []
    }
  };

  setInputValue = (type: string, inputValue: string) => {
    if (type == 'country') {
      this.setState({countryName: inputValue})
    } else if (type == 'state') {
      this.setState({stateName: inputValue})
    } else if (type == 'city') {
      this.setState({cityName: inputValue})
    }
  }

  // componentWillUpdate(nextProps: any) {
  //   if (nextProps.data != this.props.data) {
  //     debugger
  //     const {data: {
  //       country_name,
  //       state_name,
  //       city_name,
  //       country,
  //       state,
  //       city
  //     }} = nextProps;
  //     this.setInputValue('country', country_name);
  //     this.setInputValue('state', state_name);
  //     this.setInputValue('city', city_name);
  //     this.setLocation('country', country);
  //     this.setLocation('state', state);
  //     this.setLocation('city', city);
  //   }
  // }

  setLocation = (type: string, value: string) => {
    if (type == 'country') {
      this.setState({country: value})
    } else if (type == 'state') {
      this.setState({state: value})
    } else if (type == 'city') {
      this.setState({city: value})
    }
  }

  handleSubmit = (e: any) => {
    e.preventDefault();
    const { form, onSubmit, isLoading } = this.props;
    form.validateFields((error: any, value: any) => {
      if (!error) {
        value.city = this.state.city;
        value.country = this.state.country;
        value.state = this.state.state;
        if (value[this.image.name]) {
          const { uploadFile } = value[this.image.name];
          !isLoading && onSubmit(value, uploadFile);
        } else {
          !isLoading && onSubmit(value);
        }
      }
    });
  };

  componentDidUpdate(prevProps: any){
    if(prevProps.form != this.props.form){
      const {form} = this.props;
      if(form){
        const country = form.getFieldValue('country');
        const city = form.getFieldValue('city');
        const state = form.getFieldValue('state');
        const country_name = form.getFieldValue('country_name');
        const city_name = form.getFieldValue('city_name');
        const state_name = form.getFieldValue('state_name');
        debugger
        this.setInputValue('country', country_name ? country_name : '');
        this.setInputValue('state', state_name ? state_name : '');
        this.setInputValue('city', city_name ? city_name : '');
        this.setLocation('country', country ? country : '');
        this.setLocation('state', state ? state : '');
        this.setLocation('city', city ? city : '');
      }
    }
  }
  renderErrorSection = (name: string) => {
    const { getFieldError } = this.props.form;
    const errors = getFieldError(name);
    return errors
      ? errors.map((err: any, index: any) => {
          return <FormError key={index} text={err} />;
        })
      : null;
  };

  render() {
    const { getFieldProps, getFieldError } = this.props.form;
    const { isLoading, title, submitBtnTitle } = this.props;
    return (
      <div className="row">
        <div className="col-md-12">
          <form className={styles['form']} onSubmit={this.handleSubmit}>
            <div className="container">
              <h2 className="text-center">{title}</h2>
              <div className="row">
                <div className="col-12">
                  <label>{this.image.label}</label>
                  <ImagePreviewUpload {...getFieldProps(this.image.name)} />
                </div>
                {/* school name */}
                <div className="col-12 text-left">
                  <label>{this.title.label}</label>
                  <input
                    type="text"
                    placeholder={this.title.placeholder}
                    autoFocus
                    {...getFieldProps(this.title.name, this.title.options)}
                  />
                  <div>{this.renderErrorSection(this.title.name)}</div>
                </div>
                {/* degree name */}
                <div className="col-md-12 text-left">
                  <label>{this.company.label}</label>
                  <input
                    type="text"
                    placeholder={this.company.placeholder}
                    {...getFieldProps(this.company.name, this.company.options)}
                  />
                  <div>{this.renderErrorSection(this.company.name)}</div>
                </div>
                {/* location */}
                {/* <div className="col-12">
                  <EnhancedLocationSelect
                    error={getFieldError(this.places.name)}
                    {...getFieldProps(this.places.name, this.places.options)}
                  />
                </div> */}
                {/* <div className="col-12">
                  <span>{this.address.label}</span>
                  <input
                    type="text"
                    placeholder="Address"
                    {...getFieldProps(this.address.name, this.address.options)}
                  />
                  <div>{this.renderErrorSection(this.address.name)}</div>
                </div> */}

                <LocationAutoComplete
                  country={this.state.country}
                  state={this.state.state}
                  countryName={this.state.countryName}
                  stateName={this.state.stateName}
                  cityName={this.state.cityName}
                  setInputValue={this.setInputValue}
                  city={this.state.city}
                  setLocation={this.setLocation}
                  renderErrorSection={this.renderErrorSection}
                  {...getFieldProps('country')}
                  {...getFieldProps('state')}
                  {...getFieldProps('city')}
                  getFieldProps={getFieldProps}/>

                {/* month year from-to */}
                <div className="col-md-12 text-left">
                  <MonthYearRange
                    checkboxLabel="Currently Work Here"
                    {...getFieldProps(this.timeline.name, this.timeline.options)}
                  />
                  <div>{this.renderErrorSection(this.timeline.name)}</div>
                </div>
                {/* description */}
                <div className="col-md-12 text-left">
                  <label>{this.description.label}</label>
                  <textarea
                    rows={3}
                    placeholder={this.description.placeholder}
                    {...getFieldProps(this.description.name, this.description.options)}
                  />
                  <div>{this.renderErrorSection(this.description.name)}</div>
                </div>

                <div className={`col-md-12 ${styles['button-wrapper']}`}>
                  <LoadingButton isLoading={isLoading} type="submit" text={submitBtnTitle} />
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default ExperienceForm;
