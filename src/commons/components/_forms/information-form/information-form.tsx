import React, { Component } from 'react';
import { EmploymentStatusEnum, SeekingStatusEnum } from '../../../../commons/types';
import { maskPhoneInput } from '../../../../utils';
import { FormError, Modal, ImageWithButton } from '../../../../commons/components';
import { AvatarUploader } from '../../../../commons/components/';
import styles from './information-form.module.scss';
import { enhanceLocationSelect } from '../../../../HOCs';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { LocationAutoComplete } from '../../tag-input/location-autocomplete';

const EnhancedLocationSelect = enhanceLocationSelect();

interface InformationFormPropTypes {
  form: any;
  handleUploadAvatar: any;
}

class InformationForm extends Component<InformationFormPropTypes, {}> {
  // places = {
  //   name: 'places',
  //   options: {
  //     initialValue: {
  //       country: -1,
  //       state: -1,
  //       city: -1
  //     },
  //     rules: [
  //       {
  //         validator: (rule: any, value: any, callback: any) => {
  //           if (value && value.selectedCountryId === -1) {
  //             callback('Country is required');
  //           }
  //           if (value && value.selectedStateId === -1) {
  //             callback('State is required');
  //           }
  //           if (value && value.selectedCityId === -1) {
  //             callback('City is required');
  //           }
  //           callback();
  //           return;
  //         }
  //       }
  //     ]
  //   }
  // };
  address = {
    name: 'address',
    label: 'Address*',
    placeholder: 'Address',
    options: {
      initialValue: '',
      rules: [
        {
          required: true,
          message: 'Address is required',
          transform: (value: string) => value.trim()
        }
      ]
    }
  };

  job_title = {
    name: 'job_title',
    label: 'Job title',
    placeholder: 'Job title',
    options: {
      initialValue: '',
      rules: [
        {
          required: true,
          message: 'Job title is required',
          transform: (value: string) => value.trim()
        }
      ]
    }
  };

  age = {
    name: 'age',
    label: 'Date of birth',
    placeholder: 'Date of birth',
    options: {
      initialValue: '',
      // rules: [
      //   {
      //     validator: (rule: any, value: any, callback: any) => {
      //       if (value && isNaN(value) && value.toString().trim() !== '') {
      //         callback('Age must be an integer');
      //       }
      //       callback();
      //       return;
      //     }
      //   },
      //   {
      //     validator: (rule: any, value: any, callback: any) => {
      //       if (
      //         value &&
      //         !isNaN(value) &&
      //         value.toString().trim() !== '' &&
      //         (Number(value) > 99 || Number(value) < 13)
      //       ) {
      //         callback('Age must be between 13 and 99');
      //       } else {
      //         if (!Number.isInteger(Number(value))) {
      //           callback('Age must be an integer');
      //         }
      //         callback();
      //         return;
      //       }
      //     }
      //   }
      // ]
    }
  };
  phone = {
    name: 'phone',
    label: 'Phone',
    placeholder: '(XXX) XXX-XXXX',
    options: {
      initialValue: '',
      rules: [
        {
          len: 14,
          message: 'Phone number is not valid'
        }
      ]
    }
  };
  employmentStatus = {
    name: 'employment_status',
    label: 'Employment Status*',
    placeholder: 'Employment Status',
    options: {
      initialValue: 'Full Time',
      rules: [
        {
          type: 'enum',
          enum: [...Object.keys(EmploymentStatusEnum).map((key: any) => key)]
        },
        { required: true }
      ]
    }
  };
  seekingStatus = {
    name: 'seeking_status',
    label: 'Seeking Status*',
    placeholder: 'Seeking Status',
    options: {
      initialValue: SeekingStatusEnum.Active,
      rules: [
        {
          type: 'enum',
          enum: [...Object.keys(SeekingStatusEnum).map((key: any) => SeekingStatusEnum[key])]
        },
        { required: true }
      ]
    }
  };

  state = {
    src: '',
    isModalOpen: false,
    date_of_birth: new Date(),
    inValidAge: false,
    isEmptyDate: true,
    city: '',
    country: '',
    state: '',
    cityName: '',
    countryName: '',
    stateName: ''
  };

  setInputValue = (type: string, inputValue: string) => {
    if (type == 'country') {
      this.setState({countryName: inputValue})
    } else if (type == 'state') {
      this.setState({stateName: inputValue})
    } else if (type == 'city') {
      this.setState({cityName: inputValue})
    }
  }

  setLocation = (type: string, value: string) => {
    const { setFieldsValue } = this.props.form;
    console.log(this.props.form.getFieldsValue());
    debugger
    if (type == 'country') {
      this.setState({country: value})
      setFieldsValue({
        country: value,
      });
    } else if (type == 'state') {
      this.setState({state: value})
      setFieldsValue({
        state: value,
      });
    } else if (type == 'city') {
      this.setState({city: value})
      setFieldsValue({
        city: value
      })
    }
  }

  onClose = () => {
    this.setState({ preview: '' });
  };

  onCrop = (preview: string) => {
    this.setState({ preview });
  };

  renderErrorSection = (name: string) => {
    const { getFieldError } = this.props.form;
    const errors = getFieldError(name);
    return errors
      ? errors.map((err: any, index: any) => {
          return <FormError key={index} text={err} />;
        })
      : null;
  };

  maskPhoneInput = (e: any) => {
    e.target.value = maskPhoneInput(e.target.value);
  };

  toggleModal = () => {
    this.setState({ isModalOpen: !this.state.isModalOpen });
  };

  onUploadAvatar = () => {
    const { handleUploadAvatar } = this.props;
    const { setFieldsValue } = this.props.form;
    const { preview } = this.state;
    handleUploadAvatar(preview);
    setFieldsValue({
      image_url: preview
    });
    this.toggleModal();
  };
  handleChange = (date: Date) => {
    const { setFieldsValue } = this.props.form;
    if(!date){
      this.setValueBirthday(null);
      return this.setState({
        date_of_birth: null,
        isEmptyDate: true,
      });
    }
    const yearNow = new Date().getFullYear();
    const yearChange = date.getFullYear();
    const ageInfo = yearNow - yearChange;
    let value = null;
    if(ageInfo < 13 || ageInfo > 99){
      this.setState({
        inValidAge: true,
        date_of_birth: null,
        isEmptyDate: true,
      })
    }
    else{
      this.setState({
        inValidAge: false,
        date_of_birth: date,
        isEmptyDate: false,
      });
      const currentMonth = date.getMonth() + 1;
      value = date.getFullYear() + '/' + currentMonth + '/' + date.getDate();
    }
    this.setValueBirthday(value);
  }

  setValueBirthday = (val: any) => {
    const { setFieldsValue } = this.props.form;
    setFieldsValue({
      date_of_birth: val,
    });
  }

  componentDidUpdate(prevProps: any){
    if(prevProps.form != this.props.form){
      const date_of_birth= this.props.form.getFieldProps('date_of_birth');
      if(!date_of_birth.value){
        this.setState({
          date_of_birth : null,
          isEmptyDate: true,
        });
      }
      else{
        this.setState({
          date_of_birth : new Date(date_of_birth.value),
          isEmptyDate: false
        });
      }
    }
  }
  render() {
    const { getFieldProps, getFieldError } = this.props.form;
    const { src, isModalOpen,inValidAge,date_of_birth,isEmptyDate } = this.state;
    return (
      <form>
        <div className="row">
          {/* avatar */}
          <div className="col-12">
            {/* <span>Avatar</span> */}
            <Modal isOpen={isModalOpen} toggleModal={this.toggleModal}>
              <AvatarUploader src={src} onClose={this.onClose} onCrop={this.onCrop} onSubmit={this.onUploadAvatar} />
            </Modal>
            <div className={styles['avatar-wrapper']}>
              <ImageWithButton
                isFormControl={true}
                imgWidth={150}
                imgHeight={150}
                shape="round"
                btnText="edit"
                handleBtnClick={this.toggleModal}
                {...getFieldProps('image_url', {
                })}
              />
            </div>
          </div>
          {/* location */}
          {/* <div className="col-12">
            <EnhancedLocationSelect
              error={getFieldError(this.places.name)}
              {...getFieldProps(this.places.name, this.places.options)}
            />
          </div> */}
          {/* <div className="col-12">
            <span>{this.address.label}</span>
            <input type="text" placeholder="Location" {...getFieldProps(this.address.name, this.address.options)} />
            <div>{this.renderErrorSection(this.address.name)}</div>
          </div> */}

          <LocationAutoComplete
            country={this.state.country}
            state={this.state.state}
            countryName={this.state.countryName}
            stateName={this.state.stateName}
            cityName={this.state.cityName}
            setInputValue={this.setInputValue}
            city={this.state.city}
            setLocation={this.setLocation}
            renderErrorSection={this.renderErrorSection}
            getFieldProps={getFieldProps}/>


          {/* job title */}
          <div className="col-12">
            <span>{this.job_title.label}</span>
            <input
              type="text"
              placeholder={this.job_title.placeholder}
              {...getFieldProps(this.job_title.name, this.job_title.options)}
            />
            <div>{this.renderErrorSection(this.job_title.name)}</div>
          </div>
          {/* age */}
          <div className="col-12">
              <span>{this.age.label}</span>
              <div className={styles['input-age']}>
                <DatePicker
                  selected={isEmptyDate ? '' : date_of_birth}
                  dateFormat="dd/MM/yyyy"
                  onChange={this.handleChange} 
                  peekNextMonth
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                />
                {inValidAge ?
                  <span className={styles['span-age']}>Age must be between 13 and 99</span>
                : null}
                {/* <input type="text" placeholder={this.age.placeholder} {...getFieldProps(this.age.name, this.age.options)} /> */}
                <div>{this.renderErrorSection(this.age.name)}</div>
            </div>
          </div>
          {/* phone number */}
          <div className="col-12">
            <span>{this.phone.label}</span>
            <input
              type="text"
              onInput={this.maskPhoneInput}
              placeholder={this.phone.placeholder}
              {...getFieldProps(this.phone.name, this.phone.options)}
            />
            <div>{this.renderErrorSection(this.phone.name)}</div>
          </div>
          {/* Employment status */}
          <div className="col-12">
            <span>{this.employmentStatus.label}</span>
            <select
              className="custom-select"
              {...getFieldProps(this.employmentStatus.name, this.employmentStatus.options)}
            >
              {Object.keys(EmploymentStatusEnum).map((option: any, index: any) => {
                return (
                  <option key={index} value={option}>
                    {EmploymentStatusEnum[option]}
                  </option>
                );
              })}
            </select>
            <div>{this.renderErrorSection(this.employmentStatus.name)}</div>
          </div>
          {/* Seeking status */}
          <div className="col-12">
            <span>{this.seekingStatus.label}</span>
            <select className="custom-select" {...getFieldProps(this.seekingStatus.name, this.seekingStatus.options)}>
              {Object.keys(SeekingStatusEnum).map((option: any, index: any) => {
                return (
                  <option key={index} value={SeekingStatusEnum[option]}>
                    {SeekingStatusEnum[option]}
                  </option>
                );
              })}
            </select>
            <div>{this.renderErrorSection(this.seekingStatus.name)}</div>
          </div>
        </div>
      </form>
    );
  }
}

export default InformationForm;
