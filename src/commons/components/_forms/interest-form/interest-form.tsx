import React, { Component } from 'react';
import { TagInput } from '../../tag-input/tag-input';
import styles from './interest-form.module.scss';
import { FormError } from '../../form-error/form-error';
import { enhanceTagInput } from '../../../../HOCs';

const EnhancedTagInput = enhanceTagInput();

interface CertificateFormPropTypes {
  form: any;
  onSubmit: any;
  isLoading: boolean;
  title: string;
  submitBtnTitle: string;
}

class InterestForm extends Component<CertificateFormPropTypes> {
  interests = {
    name: 'interests',
    options: [
      {
        initialValue: []
      }
    ]
  };

  handleSubmit = (e: any) => {
    e.preventDefault();
    const { form, onSubmit, isLoading } = this.props;
    form.validateFields((error: any, value: any) => {
      if (!error) {
        console.log(value);
        !isLoading && onSubmit(value);
      }
    });
  };

  renderErrorSection = (name: string) => {
    const { getFieldError } = this.props.form;
    const errors = getFieldError(name);
    return errors
      ? errors.map((err: any, index: any) => {
          return <FormError key={index} text={err} />;
        })
      : null;
  };

  render() {
    const { title, form } = this.props;
    const { getFieldProps } = form;
    return (
      <React.Fragment>
        <form
          className={styles['form']}
          onSubmit={(e: any) => {
            e.preventDefault();
          }}
        >
          <div className="container">
            <h2 className="text-center">{title}</h2>
            <div className="row">
              {/* skill tag-input */}
              <div className="col-12 text-left">
                <EnhancedTagInput
                  placeHolder="Interest (ex: football)"
                  {...getFieldProps(this.interests.name, this.interests.options)}
                />
                <div>{this.renderErrorSection(this.interests.name)}</div>
              </div>
            </div>
            <div className={styles['save-button']}>
              <a className="btn btn--primary" href="#" onClick={this.handleSubmit}>
                <span className="btn__text">Save changes</span>
              </a>
            </div>
          </div>
        </form>
      </React.Fragment>
    );
  }
}

export default InterestForm;
