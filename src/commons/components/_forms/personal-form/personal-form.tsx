import React, { Component } from 'react';
import { RegexConst, RoutePaths } from '../../../../commons/constants';
import styles from './personal-form.module.scss';
import { LoadingButton } from '../../_buttons';
import { FormError } from '../../form-error/form-error';
import { maskPhoneInput } from '../../../../utils';
import { EmploymentStatusEnum, SeekingStatusEnum } from '../../../../commons/types';
import { Modal } from '../../modal/modal';
import { createForm } from 'rc-form';
import { EmailForm } from '../../_forms/email-form/email-form';
import { enhanceLocationSelect, enhanceTagInput } from '../../../../HOCs';
import { updateUserMapper } from '../../../../utils/mapper';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { userInfo } from 'os';
import { LocationAutoComplete } from '../../tag-input/location-autocomplete';
const EnhancedTagInput = enhanceTagInput();

interface PersonalFormPropsType {
  userInfo: any;
  form: any;
  actionSubmit: any;
  actionSubmitEmail: any;
  isLoading: boolean;
  errorFromServer: string;
  actionCheckEmail: any;
  checkValidUsernameAction: any;


}
const EnhancedLocationSelect = enhanceLocationSelect();
const EnhanceEmailForm = createForm()(EmailForm);

class PersonalForm extends Component<PersonalFormPropsType> {
  state = {
    isOpenEmailModal: false,
    date_of_birth: new Date(),
    inValidAge: false,
    isEmptyDate: true,
    city: '',
    country: '',
    state: '',
    cityName: '',
    countryName: '',
    stateName: ''
  };
  country = {
    label: 'Country',
    name: 'country_name',
    options: [
      {
        initialValue: []
      }
    ]
  };
  stateLocation = {
    label: 'State',
    name: 'state_name',
    options: [
      {
        initialValue: []
      }
    ]
  };
  city = {
    label: 'City',
    name: 'city_name',
    options: [
      {
        initialValue: []
      }
    ]
  };

  setInputValue = (type: string, inputValue: string) => {
    if (type == 'country') {
      this.setState({countryName: inputValue})
    } else if (type == 'state') {
      this.setState({stateName: inputValue})
    } else if (type == 'city') {
      this.setState({cityName: inputValue})
    }
  }

  setLocation = (type: string, value: string) => {
    if (type == 'country') {
      this.setState({country: value})
    } else if (type == 'state') {
      this.setState({state: value})
    } else if (type == 'city') {
      this.setState({city: value})
    }
  }

  handleOnSubmit = (e: any) => {
    e.preventDefault();
    const { form, actionSubmit } = this.props;
    form.validateFields((error: any, value: any) => {
      if (!error) {
        const {country, state, city} = this.state;
        value.country = country;
        value.state = state;
        value.city = city;
        const mappedData = updateUserMapper.formToApi(value);
        const bod = this.state.date_of_birth;
        // lib getMonth - 1;
        if(bod){
          const currentMonth = bod.getMonth() + 1;
          const dateOfBirth = bod.getFullYear() + '/' + currentMonth +  '/' + bod.getDate();
          mappedData.date_of_birth = dateOfBirth;
        }
        else{
          mappedData.date_of_birth = null;
        }
        actionSubmit(mappedData);
      }
    });
  };

  formRefEmail: any;

  toggleEmailModal = () => {
    const { userInfo } = this.props;
    this.formRefEmail.props.form.setFieldsValue({ ...userInfo });
    this.setState({
      isOpenEmailModal: !this.state.isOpenEmailModal
    });
  };

  handleEmailSubmit = (data: any) => {
    const { actionSubmitEmail, userInfo } = this.props;
    if (data.email !== userInfo.email) {
      actionSubmitEmail(data);
    }
    this.toggleEmailModal();
  };

  renderErrorSection = (name: string) => {
    const { getFieldError } = this.props.form;
    const errors = getFieldError(name);
    return errors
      ? errors.map((err: any, index: any) => {
          return <FormError key={index} text={err} />;
        })
      : null;
  };

  firstname = {
    label: 'First name*',
    type: 'text',
    name: 'first_name',
    placeholder: 'First name',
    options: {
      initialValue: '',
      rules: [
        {
          required: true,
          message: 'First name is required.',
          transform: (value: string) => value.trim()
        }
      ]
    }
  };

  lastname = {
    label: 'Last name*',
    type: 'text',
    name: 'last_name',
    placeholder: 'Last name',
    options: {
      initialValue: '',
      rules: [
        {
          required: true,
          message: 'Last name is required.',
          transform: (value: string) => value.trim()
        }
      ]
    }
  };

  username = {
    name: 'username',
    type: 'text',
    label: 'Username*',
    placeholder: 'username',
    options: {
      initialValue: '',
      rules: [
        { required: true, message: 'Username name is required' },
        {
          transform: (value: string) => value.trim(),
          validator: (rule: any, value: any, callback: any) => {
            const { checkValidUsernameAction, userInfo } = this.props;
            const conditionUsername = /^[A-Za-z0-9@.+\-_]+$/g;

            if (value === '' || value === userInfo.username) {
              callback();
              return;
            }
            if (value.match(conditionUsername)) {
              checkValidUsernameAction(value, callback);
              return;
            }
            callback('Username input contains no characters specified!');
          }
        }
      ]
    }
  };

  email = {
    label: 'Email Address',
    type: 'text',
    name: 'email',
    placeholder: 'Email',
    options: {
      initialValue: ''
    }
  };

  password = {
    label: 'Password*',
    type: 'password',
    name: 'password1',
    placeholder: 'Password',
    options: {
      initialValue: '',
      rules: [
        {
          min: 8,
          message: 'Password must be at least 8 characters'
        },
        { required: true, message: 'Password is required.' },
        {
          validator: (rule: any, value: any, callback: any) => {
            const { validateFields, isFieldTouched } = this.props.form;
            if (isFieldTouched(this.passwordConfirm.name)) {
              validateFields([this.passwordConfirm.name], {
                force: true
              });
            }
            const conditionOneLetter = RegexConst.SAME_CHARACTER_REGEX;
            const conditionPassword = RegexConst.ONLY_NUMERIC_REGEX;
            if (!value.match(conditionPassword)) {
              if (value.match(conditionOneLetter)) {
                callback('Password is too common.');
              }
              callback();
              return;
            }
            callback("Password input can't be entirely numeric!");
          }
        }
      ]
    }
  };

  passwordConfirm = {
    label: 'Confirm Password*',
    type: 'password',
    name: 'password2',
    placeholder: 'Confirm Password',
    options: {
      initialValue: '',
      rules: [
        {
          required: true,
          message: 'Password confirmation is required.'
        },
        {
          validator: (rule: any, value: any, callback: any) => {
            const { getFieldValue } = this.props.form;
            const inputPassword = getFieldValue(this.password.name);
            if (value !== inputPassword) {
              callback('Confirm password does not match!');
              return;
            }
            callback();
          }
        }
      ]
    }
  };

  // places = {
  //   name: 'places',
  //   options: {
  //     initialValue: {
  //       country: -1,
  //       state: -1,
  //       city: -1
  //     },
  //     rules: [
  //       {
  //         validator: (rule: any, value: any, callback: any) => {
  //           if (value && value.selectedCountryId === -1) {
  //             callback('Country is required');
  //           }
  //           if (value && value.selectedStateId === -1) {
  //             callback('State is required');
  //           }
  //           if (value && value.selectedCityId === -1) {
  //             callback('City is required');
  //           }
  //           callback();
  //           return;
  //         }
  //       }
  //     ]
  //   }
  // };

  address = {
    name: 'address',
    label: 'Address*',
    placeholder: 'Address',
    options: {
      initialValue: '',
      rules: [
        {
          required: true,
          message: 'Address is required',
          transform: (value: string) => value.trim()
        }
      ]
    }
  };

  age = {
    name: 'age',
    label: 'Date of birth',
    placeholder: 'Date of birth',
    options: {
      initialValue: '',
      // rules: [
      //   {
      //     validator: (rule: any, value: any, callback: any) => {
      //       if (value && isNaN(value) && value.toString().trim() !== '') {
      //         callback('Age must be an integer');
      //       }
      //       callback();
      //       return;
      //     }
      //   },
      //   {
      //     validator: (rule: any, value: any, callback: any) => {
      //       if (
      //         value &&
      //         !isNaN(value) &&
      //         value.toString().trim() !== '' &&
      //         (Number(value) > 99 || Number(value) < 13)
      //       ) {
      //         callback('Age must be between 13 and 99');
      //       } else {
      //         if (!Number.isInteger(Number(value))) {
      //           callback('Age must be an integer');
      //         }
      //         callback();
      //         return;
      //       }
      //     }
      //   }
      // ]
    }
  };
  phone = {
    name: 'phone',
    label: 'Phone',
    placeholder: '(XXX) XXX-XXXX',
    options: {
      initialValue: '',
      rules: [
        {
          len: 14,
          message: 'Phone number is not valid'
        }
      ]
    }
  };
  employmentStatus = {
    name: 'employment_status',
    label: 'Employment Status*',
    placeholder: 'Employment Status',
    options: {
      initialValue: 'Full Time',
      rules: [
        {
          type: 'enum',
          enum: [...Object.keys(EmploymentStatusEnum).map((key: any) => key)]
        },
        { required: true }
      ]
    }
  };

  seekingStatus = {
    name: 'seeking_status',
    label: 'Seeking Status*',
    placeholder: 'Seeking Status',
    options: {
      initialValue: SeekingStatusEnum.Active,
      rules: [
        {
          type: 'enum',
          enum: [...Object.keys(SeekingStatusEnum).map((key: any) => SeekingStatusEnum[key])]
        },
        { required: true }
      ]
    }
  };

  handleSelect = (date : Date) => {
  }
  handleChange = (date: Date) => {
    if(!date){
      return  this.setState({
        date_of_birth: null,
        isEmptyDate: true,
      })
    }
    const yearNow = new Date().getFullYear();
    const yearChange = date.getFullYear();
    const ageInfo = yearNow - yearChange;
    if(ageInfo < 13 || ageInfo > 99){
      this.setState({
        inValidAge: true,
        date_of_birth: null,
        isEmptyDate: true,
      })
    }
    else{
      this.setState({
        inValidAge: false,
        date_of_birth: date,
        isEmptyDate: false
      });
    }
  }

  componentDidUpdate(prevProps: any){
    if(prevProps.userInfo != this.props.userInfo){
      const {userInfo} = this.props;
      if(!userInfo.date_of_birth){
        this.setState({
          date_of_birth : null,
          isEmptyDate: true,
        });
      }
      else{
        this.setState({
          date_of_birth : new Date(userInfo.date_of_birth),
          isEmptyDate: false
        });
      }
    }
  }

  componentWillUpdate(nextProps: any) {
    if (nextProps.userInfo != this.props.userInfo) {
      debugger
      const {userInfo: {
        country_name,
        state_name,
        city_name,
        state,
        city,
        country
      }} = nextProps;
      this.setInputValue('country', country_name);
      this.setInputValue('state', state_name);
      this.setInputValue('city', city_name);
      this.setLocation('country', country);
      this.setLocation('state', state);
      this.setLocation('city', city);
    }
  }

  maskPhoneInput = (e: any) => {
    e.target.value = maskPhoneInput(e.target.value);
  };
  render() {
    const {
      userInfo,
      form: { getFieldProps },
      isLoading,
      actionCheckEmail,
      errorFromServer
    } = this.props;
    const { getFieldError } = this.props.form;
    const {
      isOpenEmailModal,
      inValidAge,
      isEmptyDate
    } = this.state;
    return (
      <div className="row d-flex justify-content-center">
        <div className="col-sm-12 col-md-12 col-lg-12">
          <div className="switchable__text">
            <form onSubmit={this.handleOnSubmit}>
              <div className="row">
                <div className="col-lg-6 col-md-12">
                  <span>{this.firstname.label}</span>
                  <input
                    type={this.firstname.type}
                    placeholder={this.firstname.placeholder}
                    autoFocus
                    tabIndex={1}
                    {...getFieldProps(this.firstname.name, this.firstname.options)}
                  />
                  {this.renderErrorSection(this.firstname.name)}
                </div>
                <div className="col-lg-6 col-md-12">
                  <span>{this.lastname.label}</span>
                  <input
                    type={this.lastname.type}
                    placeholder={this.lastname.placeholder}
                    tabIndex={2}
                    {...getFieldProps(this.lastname.name, this.lastname.options)}
                  />
                  {this.renderErrorSection(this.lastname.name)}
                </div>

                <div className={`col-12 col-md-7 ${styles['email-group']}`}>
                  <span className="input-label">{this.email.label}</span>
                  <div className={styles['input-email']}>
                    <span className={styles['form-email-disable']}>{userInfo && userInfo.email}</span>
                    <a href="#" onClick={this.toggleEmailModal}>
                      Edit
                    </a>
                  </div>
                  {this.renderErrorSection(this.email.name)}
                </div>
                {/* location */}
                {/* <div className="col-12">
                  <EnhancedLocationSelect
                    {...getFieldProps(this.places.name, this.places.options)}
                    error={getFieldError(this.places.name)}
                  />
                </div> */}

                {/* <span>{this.address.label}</span>
                <input
                  type="text"
                  placeholder={this.address.placeholder}
                  {...getFieldProps(this.address.name, this.address.options)}
                />
                <div>{this.renderErrorSection(this.address.name)}</div> */}
                
                <LocationAutoComplete
                 country={this.state.country}
                 state={this.state.state}
                 countryName={this.state.countryName}
                 stateName={this.state.stateName}
                 cityName={this.state.cityName}
                 setInputValue={this.setInputValue}
                 city={this.state.city}
                 setLocation={this.setLocation}
                 renderErrorSection={this.renderErrorSection}
                 getFieldProps={getFieldProps}/>


                {/* age */}
                <div className="col-12">
                  <span>{this.age.label}</span>
                  {/* <input
                    type="text"
                    placeholder={this.age.placeholder}
                    tabIndex={4}
                    {...getFieldProps(this.age.name, this.age.options)}
                  /> */}
                  <div className={styles['input-email']}>
                    <DatePicker
                        selected={isEmptyDate ? '' : this.state.date_of_birth}
                        dateFormat="dd/MM/yyyy"
                        onChange={this.handleChange} 
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                    />
                    {inValidAge ?
                      <span className={styles['span-age']}>Age must be between 13 and 99</span>
                     : null}
                  </div>
                  <div>{this.renderErrorSection(this.age.name)}</div>
                </div>
                <div className="col-12">
                  <span>{this.phone.label}</span>
                  <input
                    type="text"
                    onInput={this.maskPhoneInput}
                    placeholder={this.phone.placeholder}
                    tabIndex={5}
                    {...getFieldProps(this.phone.name, this.phone.options)}
                  />
                  <div>{this.renderErrorSection(this.phone.name)}</div>
                </div>
                {/* Employment status */}
                <div className="col-12">
                  <span>{this.employmentStatus.label}</span>
                  <div className="input-select">
                    <select tabIndex={6} {...getFieldProps(this.employmentStatus.name, this.employmentStatus.options)}>
                      {Object.keys(EmploymentStatusEnum).map((option: any, index: any) => {
                        return (
                          <option key={index} value={option}>
                            {EmploymentStatusEnum[option]}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                  <div>{this.renderErrorSection(this.employmentStatus.name)}</div>
                </div>
                {/* Seeking status */}
                <div className="col-12">
                  <span>{this.seekingStatus.label}</span>
                  <div className="input-select">
                    <select tabIndex={7} {...getFieldProps(this.seekingStatus.name, this.seekingStatus.options)}>
                      {Object.keys(SeekingStatusEnum).map((option: any, index: any) => {
                        return (
                          <option key={index} value={SeekingStatusEnum[option]}>
                            {SeekingStatusEnum[option]}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                  <div>{this.renderErrorSection(this.seekingStatus.name)}</div>
                </div>
              </div>
              <div className={`${styles['padding-side']} row`}>
                <div className="col-12">
                  <FormError text={errorFromServer} />
                </div>
                <div className="col-12">
                  <LoadingButton
                    isLoading={isLoading}
                    className={`btn btn--primary type--uppercase ${styles['margin-top']}`}
                    type="submit"
                    text="Save"
                    tabIndex={7}
                    handleClick={this.handleOnSubmit}
                  />
                </div>
              </div>
            </form>
            <Modal isOpen={isOpenEmailModal} toggleModal={this.toggleEmailModal}>
              <EnhanceEmailForm
                userInfo={userInfo}
                wrappedComponentRef={(inst: any) => (this.formRefEmail = inst)}
                handleSubmitAction={this.handleEmailSubmit}
                actionCheckEmail={actionCheckEmail}
                title="Change Email Address"
              />
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

export default PersonalForm;
