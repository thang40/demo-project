import React, { Component } from 'react';
import { TagInputSkill } from '../../tag-input-skill/tag-input-skill';
import styles from './skill-form.module.scss';
import { FormError } from '../../form-error/form-error';

interface CertificateFormPropTypes {
  form: any;
  onSubmit: any;
  isLoading: boolean;
  title: string;
  submitBtnTitle: string;
}

class SkillForm extends Component<CertificateFormPropTypes> {
  skills = {
    name: 'skills',
    options: [
      {
        initialValue: []
      }
    ]
  };

  handleSubmit = (e: any) => {
    e.preventDefault();
    const { form, onSubmit, isLoading } = this.props;
    form.validateFields((error: any, value: any) => {
      if (!error) {
        const submitValue = value.skills.map((item: any) => {
          return { name: item, level: 0 };
        });
        console.log(value);
        const newValue = { skills: submitValue };
        console.log(newValue);
        !isLoading && onSubmit(newValue);
      }
    });
  };

  renderErrorSection = (name: string) => {
    const { getFieldError } = this.props.form;
    const errors = getFieldError(name);
    return errors
      ? errors.map((err: any, index: any) => {
          return <FormError key={index} text={err} />;
        })
      : null;
  };

  render() {
    const { title, form } = this.props;
    const { getFieldProps } = form;
    return (
      <React.Fragment>
        <form
          className={styles['form']}
          onSubmit={(e: any) => {
            e.preventDefault();
          }}
        >
          <div className="container">
            <h2 className="text-center">{title}</h2>
            <div className="row">
              {/* skill tag-input */}
              <div className="col-12 text-left">
                <TagInputSkill
                  placeHolder="Skill (ex: ReactJS)"
                  {...getFieldProps(this.skills.name, this.skills.options)}
                />
                <div>{this.renderErrorSection(this.skills.name)}</div>
              </div>
            </div>
            <div className={styles['save-button']}>
              <a className="btn btn--primary" href="#" onClick={this.handleSubmit}>
                <span className="btn__text">Save changes</span>
              </a>
            </div>
          </div>
        </form>
      </React.Fragment>
    );
  }
}

export default SkillForm;
