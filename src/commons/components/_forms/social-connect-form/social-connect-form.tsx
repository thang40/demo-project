import React, { Component } from 'react';
import { FormError } from '../../../../commons/components';

interface SocialConnectFormPropTypes {
  form: any;
  userInfo: any;
  listSocial: {
    linkedin: any,
    facebook: any,
    twitter: any,
    instagram: any
  }
  setLinkSocial: any,
}

class SocialConnectForm extends Component<SocialConnectFormPropTypes> {
  
  state = {
    linkedinUrl: '',
    facebookUrl: '',
    twitterUrl:'',
    instagramUrl:'',
  };
  constructor(props: any) {
    super(props);
    
  }
  listSocial= {
    linkedin: '',
    facebook: '',
    twitter: '',
    instagram: ''
  }
  // listSocial: any;
  linkedDefault = 'https://linkedin.com/in/';
  facebookDefault ='https://www.facebook.com/';
  twitterDefault = 'https://twitter.com/';
  instagramDefault= 'https://www.instagram.com/';
  
  componentDidUpdate(prevProps: any){
    if(prevProps.userInfo != this.props.userInfo){
      const {userInfo:{facebook,instagram,twitter,linkedin},setLinkSocial} = this.props;
      this.setState({
        linkedinUrl: linkedin,
        facebookUrl: facebook,
        twitterUrl:twitter,
        instagramUrl:instagram,
      })
      this.listSocial.linkedin =linkedin;
      this.listSocial.facebook =facebook;
      this.listSocial.twitter =twitter;
      this.listSocial.instagram =instagram;
      setLinkSocial(this.listSocial);
    }
  }
  changeLink = (type:string, event: any) => {
    switch (type) {
      case 'linkedin':
        this.listSocial.linkedin = event.target.value;
        this.setState({linkedinUrl: event.target.value});
        break;
      case 'facebook':
        this.listSocial.facebook = event.target.value;
        this.setState({facebookUrl: event.target.value});
        break;
      case 'twitter':
        this.listSocial.twitter = event.target.value;
        this.setState({twitterUrl: event.target.value});
        break;
        case 'instagram':
        this.listSocial.instagram = event.target.value;
        this.setState({instagramUrl: event.target.value});
        break;
      default:
        return
    }
  }
  checkLink =  (type:string,event: any) => {
    switch (type) {
      case 'linkedin':
        if(this.state.linkedinUrl == this.linkedDefault)
         this.setState({linkedinUrl: ''});
        break;
      case 'facebook':
        if(this.state.facebookUrl == this.facebookDefault)
          this.setState({facebookUrl: ''});
        break;
      case 'twitter':
        if(this.state.twitterUrl == this.twitterDefault)
          this.setState({twitterUrl: ''});
        break;
      case 'instagram':
        if(this.state.instagramUrl == this.instagramDefault)
          this.setState({instagramUrl: ''});
        break;
      default:
        return
    }
    const {setLinkSocial} = this.props
    setLinkSocial(this.listSocial);

  }
  focusLink = (type: string) => {
    switch (type) {
      case 'linkedin':
        if(this.state.linkedinUrl == ''){
          return this.setState({
            linkedinUrl: this.linkedDefault,
          });
        }
        break;
      case 'facebook':
        if(this.state.facebookUrl == ''){
          return this.setState({
            facebookUrl: this.facebookDefault,
          });
        }
        break;
      case 'twitter':
        if(this.state.twitterUrl == ''){
          return this.setState({
            twitterUrl: this.twitterDefault,
          });
        }
        break;
      case 'instagram':
        if(this.state.instagramUrl == ''){
          return this.setState({
            instagramUrl: this.instagramDefault,
          });
        }
        break;
      default:
        return;
    }
  }
  portfolio = {
    name: 'portfolio',
    label: 'Portfolio',
    placeholder: 'http://your-portfolio.com',
    options: {
      initialValue: '',
      rules: []
    }
  };
  linkedin = {
    name: 'linkedin',
    label: 'Linkedin',
    placeholder: 'https://linkedin.com/in/username',
    options: {
      initialValue: '',
      rules: []
    }
  };
  instagram = {
    name: 'instagram',
    label: 'Instagram',
    placeholder: 'https://www.instagram.com/username',
    options: {
      initialValue: '',
      rules: []
    }
  };
  facebook = {
    name: 'facebook',
    label: 'Facebook',
    placeholder: 'https://www.facebook.com/profile.php?id',
    options: {
      initialValue: '',
      rules: []
    }
  };
  twitter = {
    name: 'twitter',
    label: 'Twitter',
    placeholder: 'https://twitter.com/username',
    options: {
      initialValue: '',
      rules: []
    }
  };

  renderErrorSection = (name: string) => {
    const { getFieldError } = this.props.form;
    const errors = getFieldError(name);
    return errors
      ? errors.map((err: any, index: any) => {
          return <FormError key={index} text={err} />;
        })
      : null;
  };

  render() {
    const { getFieldProps } = this.props.form;
    return (
      <form>
        <div className="row">
          {/* portfolio */}
          <div className="col-12">
            <span>{this.portfolio.label}</span>
            <input
              type="string"
              placeholder={this.portfolio.placeholder}
              {...getFieldProps(this.portfolio.name, this.portfolio.options)}
            />
            <div>{this.renderErrorSection(this.portfolio.name)}</div>
          </div>
          {/* linkedin */}
          <div className="col-12">
            <span>{this.linkedin.label}</span>
            <input
              onFocus={() => {this.focusLink('linkedin')}}
              onChange= {(e) => this.changeLink('linkedin',e)}
              value= {this.state.linkedinUrl}
              onBlur= {(e) => this.checkLink('linkedin',e)}
              type="string"
              placeholder={this.linkedin.placeholder}
              // {...getFieldProps(this.linkedin.name, this.linkedin.options)}
            />
            <div>{this.renderErrorSection(this.linkedin.name)}</div>
          </div>
          {/* instagram */}
          <div className="col-12">
            <span>{this.instagram.label}</span>
            <input
              onFocus={() => {this.focusLink('instagram')}}
              onChange= {(e) => this.changeLink('instagram',e)}
              value= {this.state.instagramUrl}
              onBlur= {(e) => this.checkLink('instagram',e)}
              type="string"
              placeholder={this.instagram.placeholder}
              // {...getFieldProps(this.instagram.name, this.instagram.options)}
            />
            <div>{this.renderErrorSection(this.instagram.name)}</div>
          </div>
          {/* facebook */}
          <div className="col-12">
            <span>{this.facebook.label}</span>
            <input
              onFocus={() => {this.focusLink('facebook')}}
              onChange= {(e) => this.changeLink('facebook',e)}
              value= {this.state.facebookUrl}
              onBlur= {(e) => this.checkLink('facebook',e)}
              type="string"
              placeholder={this.facebook.placeholder}
              // {...getFieldProps(this.facebook.name, this.facebook.options)}
            />
            <div>{this.renderErrorSection(this.facebook.name)}</div>
          </div>
          {/* twitter */}
          <div className="col-12">
            <span>{this.twitter.label}</span>
            <input
              onFocus={() => {this.focusLink('twitter')}}
              onChange= {(e) => this.changeLink('twitter',e)}
              value= {this.state.twitterUrl}
              onBlur= {(e) => this.checkLink('twitter',e)}
              type="string"
              placeholder={this.twitter.placeholder}
              // {...getFieldProps(this.twitter.name, this.twitter.options)}
            />
            <div>{this.renderErrorSection(this.twitter.name)}</div>
          </div>
        </div>
      </form>
    );
  }
}

export default SocialConnectForm;
