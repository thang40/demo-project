import React from 'react';
import styles from './certification-list-item.module.scss';
import { buildMonthYearStr } from '../../../../utils';

interface CertificationListItemPropTypes {
  data: any;
  onEdit?: any;
  onDelete?: any;
  mode?: 'readonly' | 'editable';
}

export const CertificationListItem = (props: CertificationListItemPropTypes) => {
  const handleEdit = () => {
    const { data, onEdit } = props;
    onEdit(data.id, data);
  };

  const handleDelete = () => {
    const { data, onDelete } = props;
    onDelete(data.id);
  };

  const {
    title,
    description,
    currently_work_here,
    to_date_month,
    to_date_year,
    from_date_month,
    from_date_year,
    image_url
  } = props.data;
  const { mode } = props;
  return (
    <div className={` ${styles['certification-item']}`}>
      <div className="row">
        <div className="col-2">
          {/* sample image, remove when using real data */}
          <img className={styles['image-style']} src={image_url} />
        </div>
        <div className="col-10">
          <h5 className={styles['title']}>
            {title}
            {mode === 'editable' ? (
              <React.Fragment>
                <span className={styles['margin-left']}>|</span>
                <span className={styles['icon-wrapper']}>
                  <i className="far fa-edit" onClick={handleEdit} />
                </span>
                <span className={styles['icon-wrapper']}>
                  <i className="far fa-trash-alt" onClick={handleDelete} />
                </span>
              </React.Fragment>
            ) : null}
          </h5>
          <p className="no-margin-bottom demo-xs-font">
            {from_date_month} {from_date_year}
          </p>
          <p className={`demo-sm-font ${styles['description']}`}>{description}</p>
        </div>
      </div>
    </div>
  );
};
