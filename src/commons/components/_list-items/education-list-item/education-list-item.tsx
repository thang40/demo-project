import React from 'react';
import styles from './education-list-item.module.scss';
import { buildMonthYearStr } from '../../../../utils';

interface EducationListItemPropTypes {
  data: any;
  onEdit?: any;
  onDelete?: any;
  mode?: 'readonly' | 'editable';
}

export const EducationListItem = (props: EducationListItemPropTypes) => {
  const {
    school_name,
    description,
    from_date_year,
    from_date_month,
    to_date_month,
    to_date_year,
    degree_name,
    currently_work_here,
    image_url
  } = props.data;
  const { mode = 'editable' } = props;

  const handleEdit = () => {
    const { data, onEdit } = props;
    onEdit(data.id, data);
  };

  const handleDelete = () => {
    const { data, onDelete } = props;
    onDelete(data.id);
  };

  return (
    <div className={` ${styles['education-item']}`}>
      <div className="row">
        <div className="col-2">
          {/* sample image, remove when using real data */}
          <img className={styles['image-style']} src={image_url} />
        </div>
        <div className="col-10">
          <h5 className={styles['title']}>
            {school_name} | {degree_name}
            {mode === 'editable' ? (
              <React.Fragment>
                <span className={styles['margin-left']}>|</span>
                <span className={styles['icon-wrapper']}>
                  <i className="far fa-edit" onClick={handleEdit} />
                </span>
                <span className={styles['icon-wrapper']}>
                  <i className="far fa-trash-alt" onClick={handleDelete} />
                </span>
              </React.Fragment>
            ) : null}
          </h5>
          <p className="no-margin-bottom demo-xs-font">
            {buildMonthYearStr(currently_work_here, from_date_month, from_date_year, to_date_month, to_date_year)}
          </p>

          <p className={`no-margin-bottom demo-sm-font ${styles['description']}`}>{description}</p>
        </div>
      </div>
    </div>
  );
};
