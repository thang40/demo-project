import React from 'react';
import styles from './experience-list-item.module.scss';
import { buildMonthYearStr } from '../../../../utils';

interface ExperienceListItemPropTypes {
  data: any;
  onEdit?: any;
  onDelete?: any;
  mode?: 'readonly' | 'editable';
}

export const ExperienceListItem = (props: ExperienceListItemPropTypes) => {
  const {
    title,
    company,
    description,
    currently_work_here,
    from_date_month,
    to_date_month,
    from_date_year,
    to_date_year,
    image_url
  } = props.data;
  const { mode } = props;
  const handleEdit = () => {
    const { data, onEdit } = props;
    onEdit(data.id, data);
  };
  const handleDelete = () => {
    const { data, onDelete } = props;
    onDelete(data.id);
  };
  return (
    <div className="row">
      <div className="col-2">
        {/* sample image, remove when using real data */}
        <img className={styles['image-style']} src={image_url} />
      </div>
      <div className="col-10">
        <div className={styles['description-wrapper']}>
          <h5 className={styles['title']}>
            {title} | {company} |{' '}
            {buildMonthYearStr(currently_work_here, from_date_month, from_date_year, to_date_month, to_date_year)}
            {mode === 'editable' ? (
              <React.Fragment>
                <span className={styles['margin-left']}>|</span>
                <span className={styles['icon-wrapper']}>
                  <i className="far fa-edit" onClick={handleEdit} />
                </span>
                <span className={styles['icon-wrapper']}>
                  <i className="far fa-trash-alt" onClick={handleDelete} />
                </span>{' '}
              </React.Fragment>
            ) : null}
          </h5>
          <p className="demo-sm-font">{description}</p>
        </div>
      </div>
    </div>
  );
};
