import React from 'react';
import styles from './interest-list-item.module.scss';

interface InterestListItemPropTypes {
  data: any;
}

export const InterestListItem = (props: InterestListItemPropTypes) => {
  const { data } = props;
  return (
    <div className={`${styles['interest-item']}`}>
      <img className="no-margin-bottom" height={30} src={data.image_url} />
      <p className="no-margin-bottom demo-sm-font">{data.name}</p>
    </div>
  );
};
