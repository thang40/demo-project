import React from 'react';
import styles from './skill-list-item.module.scss';

interface SkillListItemPropTypes {
  data: any;
}

export const SkillListItem = (props: SkillListItemPropTypes) => {
  const { data } = props;
  return (
    <div className={`${styles['skill-item']}`}>
      <p className="no-margin-bottom demo-sm-font">● {data.name}</p>
    </div>
  );
};
