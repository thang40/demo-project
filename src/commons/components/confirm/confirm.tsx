import React, { Component } from 'react';
import styles from './confirm.module.scss';

interface ConfirmPropTypes {
  message: string;
  hasOk?: boolean;
  hasCancel?: boolean;
  actionOk?: any;
  actionCancel?: any;
}
export class Confirm extends Component<ConfirmPropTypes, any> {
  render() {
    const { message, actionOk, actionCancel } = this.props;
    return (
      <div className={`col-12 ${styles['content-confirm']}`}>
        <h2 className={styles['message']}>{message}</h2>
        <div className="d-flex flex-row justify-content-center">
          <a className={`btn btn-danger type--uppercase ${styles['confirm-button']} ${styles['no-margin-bottom']}`} onClick={actionOk}>
            <span>Ok</span>
          </a>
          <a className={`btn type--uppercase ${styles['cancel-button']}`} onClick={actionCancel}>
            <span>Cancel</span>
          </a>
        </div>
      </div>
    );
  }
}
