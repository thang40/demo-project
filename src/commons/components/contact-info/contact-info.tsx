import React, { Component } from 'react';
import styles from './contact-info.module.scss';
import { Link } from 'react-router-dom';
import { RoutePaths } from '../../constants';

interface ContactInfoPropTypes {
  userInfo: any;
  handleEditClick: any;
}

export class ContactInfo extends Component<ContactInfoPropTypes, any> {
  render() {
    const { userInfo, handleEditClick } = this.props;
    if (!userInfo) {
      return null;
    }
    return (
      <div className={`no-margin-bottom  col-12 boxed ${styles['form-width']}`}>
        <div className="d-flex justify-content-between">
          <h3 className={styles['title-weight']}>Contact Info</h3>
          <span onClick={handleEditClick}>
            <i className={`${styles['icon-edit']} far fa-edit`} />
          </span>
        </div>
        <div className={`text-block`}>
          <h5 className={styles['title-weight']}>Profile Url</h5>
          <Link className={styles['link-color']} to={RoutePaths.USER_PROFILE.getPath(userInfo.username)}>
            {`https://demo.com/user/${userInfo.username}`}
          </Link>
        </div>
        <hr className={styles['hr-color']} />
        <div className="text-block">
          <h5>Linked Sites</h5>
          <div className={`list-inline ${styles['link-wrapper']}`}>
            {userInfo.linkedin ? (
              <a className={styles['social-link']} href={userInfo.linkedin} target="_blank">
                <i className={` socicon-linkedin ${styles['icon-style']}`} />
              </a>
            ) : null}
            {userInfo.facebook ? (
              <a className={styles['social-link']} href={userInfo.facebook} target="_blank">
                <i className={` socicon-facebook ${styles['icon-style']} `} />
              </a>
            ) : null}
            {userInfo.twitter ? (
              <a className={styles['social-link']} href={userInfo.twitter} target="_blank">
                {' '}
                <i className={` socicon-twitter ${styles['icon-style']} `} />
              </a>
            ) : null}
            {userInfo.instagram ? (
              <a className={styles['social-link']} href={userInfo.instagram} target="_blank">
                <i className={` socicon-instagram ${styles['icon-style']}`} />
              </a>
            ) : null}
            {userInfo.portfolio ? (
              <a className={styles['social-link']} href={userInfo.portfolio} target="_blank">
                <i className={` socicon-internet ${styles['icon-style']} `} />
              </a>
            ) : null}
          </div>
        </div>
        <hr className={styles['hr-color']} />
        <div className="text-block">
          <h5>Phone & Email</h5>
          <p className="no-margin-bottom">{userInfo.phone}</p>
          <p className="no-margin-bottom">{userInfo.email}</p>
        </div>
      </div>
    );
  }
}
