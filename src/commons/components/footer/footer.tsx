import React from 'react';
import { Link } from 'react-router-dom';
import { RoutePaths } from '../../constants';

export const Footer = () => {
  return (
    <footer className="footer-7 text-center-xs  bg--secondary footer-border">
      <div className="container">
        <div className="row">
          <div className="col-md-8">
            <span className="type--fine-print">
              ©<span className="update-year">2018</span> demo — All Rights Reserved
            </span>
            <Link className="type--fine-print" to={RoutePaths.POLICY}>
              Privacy Policy
            </Link>
            <Link className="type--fine-print" to={RoutePaths.TERM_AND_CONDITION}>
              Terms and Conditions
            </Link>
            <a className="type--fine-print" href="#">
              Help
            </a>
            <Link className="type--fine-print" to={RoutePaths.CONTACT_US}>
              Contact Us
            </Link>
          </div>
          <div className="col-md-4 text-sm-center text-md-right">
            <ul className="social-list list-inline">
              <li>
                <a href="#">
                  <i className="socicon socicon-google icon icon--xs" />
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="socicon socicon-twitter icon icon--xs" />
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="socicon socicon-facebook icon icon--xs" />
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="socicon socicon-instagram icon icon--xs" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
};
