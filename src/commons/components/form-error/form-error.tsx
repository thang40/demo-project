import React from 'react';

interface FormErrorPropTypes {
  text: string;
}

export const FormError = (props: FormErrorPropTypes) => <div className="text-danger">{props.text}</div>;
