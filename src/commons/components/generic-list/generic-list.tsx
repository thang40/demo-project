import React from 'react';
import styles from './generic-list.module.scss';

interface GenericListPropTypes {
  data: any[];
  title: string;
  mode?: 'readonly' | 'editable';
  ListItemComponent: any;
  hasIcon?: boolean;
  listItemProps?: any;
  onIconClick?: any;
  onIconClick2?: any;
  itemClassName?: string;
  iconClass?: string;
  titleUpperCase?: boolean;
  LoadingComponent?: any;
  isLoading?: boolean;
  WrapperComponent?: any;
  classComponent?: string;
  isShowTitle?: boolean;
  toggleModal?: any;

  viewVideo?: any;
  isShowUploadButton?: boolean;
  isShowButtonVideo?: boolean;
  onPressUploadVideo?: any;
}

export const GenericList = (props: GenericListPropTypes) => {
  const {
    viewVideo,
    isShowUploadButton,
    isShowButtonVideo,
    onPressUploadVideo,

    data,
    title,
    mode = 'editable',
    classComponent = '',
    ListItemComponent,
    listItemProps,
    hasIcon = true,
    onIconClick = () => {},
    onIconClick2 = () => {},
    itemClassName,
    iconClass = 'far fa-edit',
    titleUpperCase = true,
    isShowTitle = true,
    toggleModal = () => {},
    LoadingComponent = <div>...loading</div>,
    WrapperComponent = (props: any) => {
      return <div>{props.children}</div>;
    },
    isLoading = false
  } = props;
  const renderIcon = () => {
    return classComponent == '' ? (
      <a className={styles['icon-a']} onClick={mode === 'editable' ? onIconClick : onIconClick2}>
        <i className={`${styles['icon']} ${iconClass}`} />
      </a>
    ) : (
      <a className={styles['icon-a']} onClick={mode === 'editable' ? onIconClick : onIconClick2}>
        Add
      </a>
    );
  };

  const renderItems = () => {
    return data && data.length ? (
      data.map((item, index) => (
        <div key={index} className={`${styles['item-wrapper']} ${itemClassName}`}>
          <ListItemComponent data={item} {...listItemProps} mode={mode} />
        </div>
      ))
    ) : (
      <div className={`${styles[classComponent]} ${styles['no-results']}`}>
        <div className="col-12 col-md-12">
          <p>No {title.toLowerCase()}.</p>
        </div>
      </div>
    );
  };

  const render = () => {
    return (
      <WrapperComponent>
        <div
          className={`${styles[classComponent]} ${
            styles['title-wrapper']
          } d-flex flex-row justify-content-between align-items-center`}
        >
          <div className={`col-12 col-md-12  ${styles['no-padding']}`}>
            <h5 className={`${titleUpperCase ? 'text-uppercase' : ''} ${styles['title']}`}>
              {isShowTitle ? title : ''}
              {classComponent == '' && isShowButtonVideo ? (
                <a>{' '}|{' '}</a>
              ): null}
              {classComponent == '' && isShowButtonVideo ? (
                <a className={styles['viewVideo']} onClick={() => viewVideo(title)}>
                  View Video
                </a>
              ) : null}
            </h5>
            <div className={styles['icon-wrapper']}>
              {classComponent == '' && isShowUploadButton ? (
                <a
                  className={styles['buttonUpload']}
                  onClick={() => {
                    toggleModal();
                    onPressUploadVideo();
                  }}
                >
                  Upload Video
                </a>
              ) : null}
              {hasIcon ? renderIcon() : null}
            </div>
          </div>
        </div>
        <div className={`row ${styles[classComponent]} ${styles['content-list']}`}>
          {isLoading ? LoadingComponent : renderItems()}
          {/* {classComponent != '' && listItemProps.isShowUploadButton ? (
            <div className={styles['buttonUploadFrame']}>
              <div className={styles['buttonUpload']}>
                <a onClick={() => {
                  toggleModal();
                  listItemProps.onPressUploadVideo();
                }}>Upload Video</a>
              </div>
            </div>
          ) : null} */}
        </div>
      </WrapperComponent>
    );
  };

  return render();
};
