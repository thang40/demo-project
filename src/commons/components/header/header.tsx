import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import styles from './header.module.scss';
import { RoutePaths } from '../../constants';

interface HeaderStateTypes {
  isMenuOpen: boolean;
  isUserOptionOpen: boolean;
}

export class Header extends Component<any, HeaderStateTypes> {
  node: any;

  state = {
    isMenuOpen: false,
    isUserOptionOpen: false
  };

  handleLogoutClick = () => {
    const { logoutAction } = this.props;
    this.setState({
      isUserOptionOpen: false
    });
    logoutAction();
  };

  handleOpenUserOption = () => {
    this.setState({
      isUserOptionOpen: !this.state.isUserOptionOpen
    });
  };

  toggleMenu = () => {
    this.setState({
      isMenuOpen: !this.state.isMenuOpen
    });
  };

  handleClickOutModal = (event: any) => {
    if (this.node.contains(event.target)) {
      return;
    }
    this.setState({
      isUserOptionOpen: false
    });
  };

  componentDidUpdate() {
    const { isUserOptionOpen } = this.state;
    if (isUserOptionOpen) {
      document.addEventListener('mousedown', this.handleClickOutModal);
    } else {
      document.removeEventListener('mousedown', this.handleClickOutModal);
    }
  }

  renderAuthenticatedMenu = () => {
    const { isUserOptionOpen } = this.state;
    const { userInfo, isAuthenticated } = this.props;
    const containerClass = isAuthenticated ? 'demo-container' : 'demo-container hide';
    return (
      <React.Fragment>
        <li className={`nav-item ${containerClass}`}>
          <div className={`nav-link dropdown ${isUserOptionOpen ? 'show' : ''}`} ref={node => (this.node = node)}>
            <a className={`${styles['clickable']}`} onClick={this.handleOpenUserOption}>
              <span>My Profile</span>
              <i className="stack-down-open" />
            </a>
            {userInfo ? (
              <ul
                className={`dropdown-menu dropdown__content ${styles['dropdown-menu']} ${
                  isUserOptionOpen ? ' show' : ''
                }`}
              >
                <li className={styles['li-username']}>
                  <NavLink
                    exact
                    className="nav-link"
                    to={RoutePaths.USER_PROFILE.getPath(userInfo && userInfo.username)}
                  >
                    Signed in as <br />
                    <strong>{userInfo && userInfo.email}</strong>
                  </NavLink>
                </li>
                <li className={styles['li-second']}>
                  <NavLink
                    exact
                    className="nav-link"
                    to={RoutePaths.USER_PROFILE.getPath(userInfo && userInfo.username)}
                  >
                    Profile
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    className="nav-link"
                    to={RoutePaths.USER_ACCOUNT_SETTING.getPath(userInfo && userInfo.username)}
                  >
                    Account Setting
                  </NavLink>
                </li>
                <li>
                  <a onClick={this.handleLogoutClick} className="nav-link">
                    Logout
                  </a>
                </li>
              </ul>
            ) : null}
          </div>
        </li>
      </React.Fragment>
    );
  };

  renderMenu = () => {
    const { isAuthenticated } = this.props;
    const containerClass = isAuthenticated ? 'demo-container hide' : 'demo-container';
    return (
      <React.Fragment>
        <li className={`nav-item ${containerClass}`}>
          <NavLink exact={true} activeClassName="active" className="nav-link" to={RoutePaths.LOGIN}>
            Login
          </NavLink>
        </li>
        <li className={`nav-item ${containerClass}`}>
          <NavLink to={RoutePaths.REGISTER} exact={true} activeClassName="active" className="nav-link">
            Register
          </NavLink>
        </li>
      </React.Fragment>
    );
  };

  render() {
    const { isMenuOpen } = this.state;
    const { isAuthenticated } = this.props;
    const containerClass = isAuthenticated === undefined ? 'demo-container hide' : 'demo-container';
    return (
      <header>
        <nav className={`${styles['nav']} navbar fixed-top navbar-expand-sm navbar-light bg-light`}>
          <NavLink exact={true} activeClassName="active" to={RoutePaths.INDEX} className="nav-link">
            <img className={styles['logo']} alt="logo" src="/images/logo-big.png" />
          </NavLink>
          <button className="navbar-toggler no-margin-top" type="button" onClick={this.toggleMenu}>
            <span className="navbar-toggler-icon" />
          </button>
          <div className={`collapse navbar-collapse ${containerClass} ${isMenuOpen ? 'show' : ''}`}>
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item active" />
            </ul>
            <ul className="navbar-nav my-auto my-2 my-lg-0 menu-horizontal">
              {/* <li className="nav-item">
                <NavLink exact={true} activeClassName="active" className="nav-link" to={RoutePaths.INDEX}>
                  Home
                </NavLink>
              </li> */}
              {isAuthenticated ? this.renderAuthenticatedMenu() : this.renderMenu()}
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}
