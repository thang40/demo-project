import React, { Component } from 'react';
import styles from './image-preview-upload.module.scss';
interface ImagePreviewUploadPropsType {
  value?: any;
  onChange?: any;
}

export class ImagePreviewUpload extends Component<ImagePreviewUploadPropsType, any> {
  inputRef: any;
  constructor(props: any) {
    super(props);
    this.inputRef = React.createRef();
  }
  handleUploadImage = (e: any) => {
    const { onChange } = this.props;
    const imageUrl = URL.createObjectURL(e.target.files[0]);
    onChange({
      image_url: imageUrl,
      uploadFile: imageUrl
    });

    // URL.revokeObjectURL(e.target.files[0]);
  };

  handleImageClick = () => {
    this.inputRef.current.click();
  };

  render() {
    const { value } = this.props;
    return (
      <div className="">
        <input
          type="file"
          accept="image/*"
          className={styles['hide-input']}
          onChange={this.handleUploadImage}
          ref={this.inputRef}
        />
        {value ? (
          <img className={styles['image']} onClick={this.handleImageClick} src={value.image_url} />
        ) : (
          <i className={`fas fa-camera align-self-center ${styles['icon']}`} onClick={this.handleImageClick} />
        )}
      </div>
    );
  }
}
