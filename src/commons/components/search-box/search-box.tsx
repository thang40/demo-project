import React from 'react';
import styles from './search-box.module.scss';
import { SearchType } from '../../types/view-model';

interface SearchBoxPropTypes {
  data: SearchType[];
  actionSearch: any;
}

interface SearchResultProptypes {
  data: SearchType[];
}

const SearchResult = (props: SearchResultProptypes) => {
  const { data } = props;
  return data.length ? (
    <div className={styles['search-result']}>
      {props.data.map((item: SearchType) => {
        return (
          <li key={item.id}>
            <a href="#">{item.title}</a>
          </li>
        );
      })}
    </div>
  ) : null;
};

export const SearchBox = (props: SearchBoxPropTypes) => {
  const { data, actionSearch } = props;

  const handleOnKeyUp = (e: any): void => {
    const searchText = (e.target as HTMLInputElement).value;
    actionSearch(searchText);
  };

  return (
    <div className={styles['search-box-wrapper']}>
      <div className="input-group">
        <div className="input-group-prepend">
          <span className={`${styles['icon-align']} input-group-text`}>
            <i className={`${styles['search-icon']} stack-search`} />
          </span>
        </div>
        <input
          type="text"
          className={`${styles['search-input']} form-control`}
          placeholder="Search"
          onKeyUp={handleOnKeyUp}
        />
        <div className="input-group-append">
          <span className={`${styles['icon-align']} input-group-text`}>
            <i className={`${styles['search-icon']} stack-down-open`} />
          </span>
        </div>
      </div>
      <SearchResult data={data} />
    </div>
  );
};
