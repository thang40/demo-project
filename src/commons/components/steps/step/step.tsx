import React from 'react';
import styles from './step.module.scss';

interface StepPropTypes {
  title: string;
  status?: string;
  itemWidth?: string;
}

export const Step = (props: StepPropTypes) => {
  const { title, status, itemWidth } = props;
  return (
    <li
      style={{ width: itemWidth }}
      className={status === 'current' ? `${styles['step']} ${styles['current']}` : styles['step']}
    >
      <a>{title}</a>
    </li>
  );
};
