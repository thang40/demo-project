import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import styles from './tag-input.module.scss';
import { Chip } from '../chip/chip';

const getSuggestionValue = (suggestion: any) => suggestion.name;

// Use your imagination to render suggestions.
const renderSuggestion = (suggestion: any) => (
  <div className={`${styles['input-wrapper']} d-flex flex-row align-items-center`}>
    <img className="no-margin-bottom" height={30} src={suggestion.image_url} />
    <div className={styles['input']}>{suggestion.name}</div>
  </div>
);

const renderInputComponent = (inputProps: any) => (
  <div className={styles['input-container']}>
    <i className={`${styles['icon']} fas fa-search`} />
    <input {...inputProps} />
  </div>
);

export class InputAutoComplete extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      inputValue: '',
      suggestions: []
    };
  }
  
  getSuggestions = (value: any) => {
    const {suggestCountry, suggestState, suggestCity, type } = this.props;
    if (type == 'country' || type == 'state' || type == 'city') {
      let suggestValue = [];
      if (type == 'country') {
        suggestValue = suggestCountry;
      } else if (type == 'state') {
        suggestValue = suggestState;
      } else if (type == 'city') {
        suggestValue = suggestCity;
      }
      if (!suggestValue) {
        return [];
      }
      const inputValue = value.trim().toLowerCase();
      const inputLength = inputValue.length;
      return inputLength === 0
        ? []
        : suggestValue.filter((data: any) => data.name.toLowerCase().slice(0, inputLength) === inputValue);
    }
  };

  onChange = (event: any, { newValue }: any) => {
    const {type, suggestCountry, suggestState, suggestCity} = this.props;
    let data = undefined;
    if (type == 'country') {
        data = suggestCountry.find((objectJson: any) => objectJson.name === newValue);
    } else if (type == 'city') {
        data = suggestCity.find((objectJson: any) => objectJson.name === newValue);
    } else if (type == 'state') {
        data = suggestState.find((objectJson: any) => objectJson.name === newValue);
    }
    if (data)
    this.props.setLocation(this.props.type, data.id);

    this.props.setInputValue(type, newValue);
    this.setState({
      inputValue: newValue
    });
  };

  onKeyPress = (e: any) => {
    if (e.key === 'Enter') {
    const {
        onChange,
        value,
        type,
        suggestCountry,
        suggestState,
        suggestCity
    } = this.props;
    const v = e.target.value.trim();
    if (v === '') {
        return;
    }
    let newValue = undefined;
    if (type == 'country') {
        newValue = suggestCountry.find((objectJson: any) => objectJson.name === v);
    } else if (type == 'city') {
        newValue = suggestCity.find((objectJson: any) => objectJson.name === v);
    } else if (type == 'state') {
        newValue = suggestState.find((objectJson: any) => objectJson.name === v);
    }
    if (newValue) {
        this.props.setLocation(type, newValue.id);
        this.setState({
            value: newValue.id,
            // inputValue: ''
        });
    }
    // onChange && onChange(newValue);
    }
  };

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }: any) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  render() {
    const {
      value,
      placeHolder,
      isShowLogoSearch = true,
      type,
      countryName,
      stateName,
      cityName
    } = this.props;
    const { inputValue, suggestions } = this.state;
    // Autosuggest will pass through all these props to the input.
    let valueLocation = inputValue
    if (type == 'country' ) {
      valueLocation = countryName;
    } else if (type == 'city') {
      valueLocation = cityName;
    } else if (type == 'state') {
      valueLocation = stateName;
    }
    const inputProps = {
      placeholder: placeHolder,
      value: valueLocation,
      onChange: this.onChange,
      onKeyPress: this.onKeyPress
    };

    // Finally, render it!
    return (
      <React.Fragment>
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          renderInputComponent={isShowLogoSearch ? renderInputComponent : undefined}
          inputProps={inputProps}
        />
      </React.Fragment>
    );
  }
}
