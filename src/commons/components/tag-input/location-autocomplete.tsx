import React, { Component } from 'react';
import { enhanceTagInput } from '../../../HOCs';

const EnhancedTagInput = enhanceTagInput();
export class LocationAutoComplete extends Component<any, any> {
    render() {
        return (
            <div className="col-md-12">
                <div className = "row">
                    <div className="col-md-12 col-lg-12">
                        <span>{'Country'}</span>
                        <EnhancedTagInput
                        placeHolder="Country"
                        isShowLogoSearch={false}
                        type={'country'}
                        country={this.props.country}
                        state={this.props.state}
                        countryName={this.props.countryName}
                        setInputValue={this.props.setInputValue}
                        city={this.props.city}
                        setLocation={this.props.setLocation}
                        {...this.props.getFieldProps('country')}
                        {...this.props.getFieldProps('country_name')}
                        />
                        <div>{this.props.renderErrorSection('country')}</div>
                    </div>
                    {/* state */}
                    <div className="col-12">
                        <span>{'State'}</span>
                        <EnhancedTagInput
                        placeHolder="State"
                        type={'state'}
                        country={this.props.country}
                        state={this.props.state}
                        stateName={this.props.stateName}
                        city={this.props.city}
                        setInputValue={this.props.setInputValue}
                        setLocation={this.props.setLocation}
                        isShowLogoSearch={false}
                        {...this.props.getFieldProps('state')}
                        {...this.props.getFieldProps('state_name')}
                        />
                        <div>{this.props.renderErrorSection('state')}</div>
                    </div>
                    {/* city */}
                    <div className="col-12">
                        <span>{'City'}</span>
                        <EnhancedTagInput
                        placeHolder="City"
                        cityName={this.props.cityName}
                        type={'city'}
                        setLocation={this.props.setLocation}
                        setInputValue={this.props.setInputValue}
                        country={this.props.country}
                        state={this.props.state}
                        city={this.props.city}
                        isShowLogoSearch={false}
                        {...this.props.getFieldProps('city')}
                        {...this.props.getFieldProps('city_name')}
                        />
                        <div>{this.props.renderErrorSection('city')}</div>
                    </div>
                </div>
            </div>
        )
    }
}