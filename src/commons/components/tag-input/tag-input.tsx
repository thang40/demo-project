import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import styles from './tag-input.module.scss';
import { Chip } from '../chip/chip';

const getSuggestionValue = (suggestion: any) => suggestion.name;

// Use your imagination to render suggestions.
const renderSuggestion = (suggestion: any) => (
  <div className={`${styles['input-wrapper']} d-flex flex-row align-items-center`}>
    <div className={styles['icon-interest']}>
      <img className="no-margin-bottom" height={30} src={suggestion.image_url} />
    </div>
    <div className={styles['input']}>{suggestion.name}</div>
  </div>
);

const renderInputComponent = (inputProps: any) => (
  <div className={styles['input-container']}>
    <i className={`${styles['icon']} fas fa-search`} />
    <input {...inputProps} />
  </div>
);

export class TagInput extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      inputValue: '',
      suggestions: []
    };
  }

  getSuggestions = (value: any) => {
    const { suggestData } = this.props;
    if (!suggestData) {
      return [];
    }
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;
    return inputLength === 0
      ? []
      : suggestData.filter((data: any) => data.name.toLowerCase().slice(0, inputLength) === inputValue);
  };

  onChange = (event: any, { newValue }: any) => {
    this.setState({
      inputValue: newValue
    });
  };

  onKeyPress = (e: any) => {
    if (e.key === 'Enter') {
      const { onChange, value } = this.props;
      const v = e.target.value.trim();
      if (v === '') {
        return;
      }
      if (value.indexOf(v) > -1) {
        return;
      }
      const newValue = [...value, v];
      this.setState({
        value: newValue,
        inputValue: ''
      });
      onChange && onChange(newValue);
    }
  };

  removeTag = (v: string) => {
    const { value, onChange } = this.props;
    const newValues = [...value];
    const index = newValues.indexOf(v);
    if (index > -1) {
      newValues.splice(index, 1);
    }
    onChange && onChange(newValues);
  };

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }: any) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  render() {
    const { value, placeHolder } = this.props;
    const { inputValue, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: placeHolder,
      value: inputValue,
      onChange: this.onChange,
      onKeyPress: this.onKeyPress
    };

    // Finally, render it!
    return (
      <React.Fragment>
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          renderInputComponent={renderInputComponent}
          inputProps={inputProps}
        />
        <div className={styles['chip-parent']}>
          {value &&
            value.map((v: any, index: number) => {
              if (typeof v === 'string') {
                return (
                  <div key={index} className={styles['chip-wrapper']}>
                    <Chip text={v} handleIconClick={() => this.removeTag(v)} />
                  </div>
                );
              }
              return (
                <div key={index} className={styles['chip-wrapper']}>
                  <Chip text={v.name} handleIconClick={() => this.removeTag(v)} />
                </div>
              );
            })}
        </div>
      </React.Fragment>
    );
  }
}
