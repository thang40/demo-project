import React, { Component } from 'react';
import { AvatarUploader } from '../avatar-uploader/avatar-uploader';
import { Modal } from '../modal/modal';
import styles from './user-info.module.scss';
import { ImageWithButton, ToggleButton } from '../_buttons';
import { EditInline } from '../edit-inline/edit-inline';

interface UserInfoPropTypes {
  userInfo: any;
  handleUploadAvatar: any;
  handleChangeUserInfo: any;
  handleUpdateJobTitle: any;
  handleUpdateStatusOpportunity: any;
}

interface UserInfoStateTypes {
  preview: string;
  src: string;
  isModalOpen: boolean;
}

export class UserInfo extends Component<UserInfoPropTypes, UserInfoStateTypes> {
  state = {
    preview: '',
    src: '',
    isModalOpen: false
  };

  onClose = () => {
    this.setState({ preview: '' });
  };

  onCrop = (preview: string) => {
    this.setState({ preview });
  };

  toggleModal = () => {
    const { isModalOpen } = this.state;
    this.setState({ isModalOpen: !isModalOpen });
  };

  onSubmitAvatar = () => {
    const { handleUploadAvatar } = this.props;
    handleUploadAvatar(this.state.preview);
    this.toggleModal();
  };

  toggleOpportunity = (e: any) => {
    const value = (e.target as HTMLInputElement).checked;
    const { handleUpdateStatusOpportunity } = this.props;
    handleUpdateStatusOpportunity({ status_opportunity: value });
  };

  updateJobTitle = (value: any) => {
    const { handleUpdateJobTitle } = this.props;
    handleUpdateJobTitle({ job_title: value });
  };

  render() {
    if (!this.props.userInfo) {
      return null;
    }

    const {
      userInfo: { first_name, last_name, job_title, image_url, status_opportunity, city_name, country_name, state_name }
    } = this.props;
    const { src, isModalOpen } = this.state;
    const placeWork = city_name + ' ' + state_name + ' ' + country_name;
    return (
      <div className={styles['modified-container']}>
        <div className="row align-items-center no-gutters">
          <div className="text-lg-left text-md-center text-sm-center col-lg-5 col-md-2 col-sm-3 col-12">
            <ImageWithButton
              imgWidth={80}
              imgHeight={80}
              shape="round"
              btnText="edit"
              handleBtnClick={this.toggleModal}
              src={image_url}
            />
            <Modal isOpen={isModalOpen} toggleModal={this.toggleModal}>
              <AvatarUploader src={src} onClose={this.onClose} onCrop={this.onCrop} onSubmit={this.onSubmitAvatar} />
            </Modal>
          </div>
          <div className="col-lg-7 col-md-10 col-sm-9 col-12">
            <h5 className="no-margin-bottom">{`${first_name} ${last_name}`}</h5>
            <EditInline value={job_title} handleChangeValue={this.updateJobTitle} />
            {placeWork && placeWork.trim() ? <p className={styles['place-work']}>{placeWork}</p> : null}
          </div>
        </div>
        <div className={`row align-items-center no-gutters ${styles['margin-top']}`}>
          <div className={`text-lg-center text-md-center text-sm-center col-12 col-md-12 ${styles['status-user']}`}>
            <ToggleButton handleToggle={this.toggleOpportunity} isChecked={status_opportunity} />
            {status_opportunity ? (
              <p className={styles['text']}>Currently looking for opportunity</p>
            ) : (
              <p className={styles['text']}>Currently not looking for opportunity</p>
            )}
          </div>
        </div>
      </div>
    );
  }
}
