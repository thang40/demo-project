import React, { Component } from 'react';
import { UserInfo } from '../user-info/user-info';
import styles from './user-sidebar.module.scss';
import { GenericList } from '../generic-list/generic-list';
import { ContactInfo } from '../contact-info/contact-info';
import { ContactInfoForm } from '../_forms/contact-info-form/contact-info-form';
import { AboutForm } from '../_forms/about-form/about-form';
import { createForm } from 'rc-form';
import { Modal } from '../modal/modal';
import ShowMoreText from 'react-show-more-text';

const EnhanceContactInfoForm = createForm()(ContactInfoForm);
const EnhanceAboutForm = createForm()(AboutForm);

interface JobOpeningItemPropTypes {
  sections: any[];
  userInfo: any;
  logoutAction: any;
  handleUploadAvatar: any;
  handleUpdateUserInfo?: any;
  handleUpdateAbout?: any;
  handleUpdateJobTitle?: any;
  handleUpdateStatusOpportunity?: any;
  checkValidUsernameAction?: any;
  history?: any;
  match?: any;
}

export class UserSidebar extends Component<JobOpeningItemPropTypes> {
  formRefContactInfo: any;
  formRefAbout: any;
  state = {
    isOpenContactInfoModal: false,
    isOpenEditContactInfoModal: false,
    isOpenAboutModal: false
  };

  toggleContactInfoModal = () => {
    const { isOpenContactInfoModal } = this.state;
    this.setState({
      isOpenContactInfoModal: !isOpenContactInfoModal
    });
  };

  toggleEditContactInfoModal = () => {
    const { userInfo } = this.props;
    const { isOpenEditContactInfoModal } = this.state;
    this.formRefContactInfo.props.form.setFieldsValue({ ...userInfo });
    this.setState({
      isOpenEditContactInfoModal: !isOpenEditContactInfoModal
    });
  };

  toggleAboutModal = () => {
    const { userInfo } = this.props;
    this.formRefAbout.props.form.setFieldsValue({ ...userInfo });
    this.setState({
      isOpenAboutModal: !this.state.isOpenAboutModal
    });
  };

  renderSections = () => {
    const { sections } = this.props;
    return sections.map((Section, i) => {
      return <React.Fragment key={i}>{Section}</React.Fragment>;
    });
  };

  handleContactInfoSubmit = (data: any) => {
    const { handleUpdateUserInfo, userInfo } = this.props;
    const isUpdatedUsername = data.username !== userInfo.username;
    handleUpdateUserInfo(data, isUpdatedUsername);
    this.toggleEditContactInfoModal();
  };

  handleAboutSubmit = (data: any) => {
    const { handleUpdateAbout } = this.props;
    handleUpdateAbout(data);
    //this.toggleAboutModal();
  };

  handleContactInfoEdit = () => {
    // this.toggleContactInfoModal();
    this.toggleEditContactInfoModal();
  };

  renderUserAbout = () => {
    const { userInfo } = this.props;
    if (!userInfo) {
      return null;
    }
    return (
      <GenericList
        data={[userInfo.about]}
        title="About"
        hasIcon={true}
        iconClass="far fa-edit"
        onIconClick={this.toggleAboutModal}
        itemClassName="col-12"
        ListItemComponent={(props: any) => {
          const { data } = props;
          return (
            <ShowMoreText lines={5} more="Show more" less="Show less" anchorClass="">
              {data}
            </ShowMoreText>
          );
        }}
        WrapperComponent={(props: any) => {
          return <div className={styles['modified-container']}>{props.children}</div>;
        }}
      />
    );
  };

  render() {
    const {
      userInfo,
      logoutAction,
      handleUploadAvatar,
      checkValidUsernameAction,
      handleUpdateUserInfo,
      handleUpdateJobTitle,
      handleUpdateStatusOpportunity
    } = this.props;
    const { isOpenContactInfoModal, isOpenEditContactInfoModal, isOpenAboutModal } = this.state;
    return (
      <div className={`sticky-top ${styles['sticky-sidebar']}`}>
        <div className={`boxed ${styles['box-shadow']} ${styles['user-info-wrapper']} bg--dark no-margin-bottom`}>
          <div className={`${styles['modified-container']}`}>
            <UserInfo
              userInfo={userInfo}
              handleUploadAvatar={handleUploadAvatar}
              handleChangeUserInfo={handleUpdateUserInfo}
              handleUpdateJobTitle={handleUpdateJobTitle}
              handleUpdateStatusOpportunity={handleUpdateStatusOpportunity}
            />
          </div>
        </div>

        <div className={`boxed ${styles['section-wrapper']} ${styles['box-shadow']}`}>
          <div className={styles['modified-container']}>
            <div className={styles['title-wrapper']}>
              <h5 className={styles['about-title']} onClick={this.toggleContactInfoModal}>
                See Contact Info
              </h5>
              <div className={`list-inline ${styles['link-wrapper']}`}>
                {userInfo && userInfo.linkedin ? (
                  <a className={styles['social-link']} href={userInfo.linkedin} target="_blank">
                    <i className={` socicon-linkedin ${styles['icon-style']}`} />
                  </a>
                ) : null}
                {userInfo && userInfo.facebook ? (
                  <a className={styles['social-link']} href={userInfo.facebook} target="_blank">
                    <i className={` socicon-facebook ${styles['icon-style']} `} />
                  </a>
                ) : null}
                {userInfo && userInfo.twitter ? (
                  <a className={styles['social-link']} href={userInfo.twitter} target="_blank">
                    {' '}
                    <i className={` socicon-twitter ${styles['icon-style']} `} />
                  </a>
                ) : null}
                {userInfo && userInfo.instagram ? (
                  <a className={styles['social-link']} href={userInfo.instagram} target="_blank">
                    <i className={` socicon-instagram ${styles['icon-style']}`} />
                  </a>
                ) : null}
                {userInfo && userInfo.portfolio ? (
                  <a className={styles['social-link']} href={userInfo.portfolio} target="_blank">
                    <i className={` socicon-internet ${styles['icon-style']} `} />
                  </a>
                ) : null}
              </div>
            </div>
          </div>

          {this.renderUserAbout()}
          <Modal isOpen={isOpenAboutModal} toggleModal={this.toggleAboutModal}>
            <EnhanceAboutForm
              wrappedComponentRef={(inst: any) => (this.formRefAbout = inst)}
              handleSubmitAction={this.handleAboutSubmit}
              title="Edit About"
            />
          </Modal>

          {this.renderSections()}

          <div className={` ${styles['modified-container']}`}>
            <a onClick={logoutAction} className={`${styles['clickable']} type--uppercase`}>
              Log out
            </a>
            <Modal isOpen={isOpenContactInfoModal} toggleModal={this.toggleContactInfoModal}>
              <ContactInfo userInfo={userInfo} handleEditClick={this.handleContactInfoEdit} />
            </Modal>
            <Modal isOpen={isOpenEditContactInfoModal} toggleModal={this.toggleEditContactInfoModal} size="big">
              <EnhanceContactInfoForm
                submitBtnTitle="Edit"
                title="Edit Contact Info"
                userInfo={userInfo}
                handleSubmit={this.handleContactInfoSubmit}
                wrappedComponentRef={(inst: any) => (this.formRefContactInfo = inst)}
                checkValidUsernameAction={checkValidUsernameAction}
                checkIsOpen={isOpenContactInfoModal}
              />
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}
