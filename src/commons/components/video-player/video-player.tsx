import React, { Component } from 'react';
import { VideoItemType } from '../../types/view-model';
import styles from './video-player.module.scss';

export interface VideoItemPropTypes {
  isShowLogo?: boolean;
  data: VideoItemType;
  height: string;
}

export class VideoPlayer extends Component<VideoItemPropTypes> {
  videoRef: any;
  state = {
    videoPlayClass: 'video-cover border--round',
    stylesFromParent: {
      backgroundImage: `url(${this.props.data.backgroundImage})`,
      opacity: 1
    }
  };

  // stylesFromParent = {
  //   backgroundImage: `url(${this.props.data.backgroundImage})`,
  //   opacity: 1
  // };

  constructor(props: VideoItemPropTypes) {
    super(props);
    this.videoRef = React.createRef();
  }

  handleOnclick = (e: any) => {
    this.setState({
      videoPlayClass: 'video-cover border--round reveal-video'
    });
    this.videoRef.current.play();
  };

  componentWillReceiveProps(props: any) {
    const { backgroundImage } = props;
    this.setState({
      stylesFromParent: {
        backgroundImage: `url(${backgroundImage})`,
        opacity: 1
      }
    });
  }

  render() {
    const { videoPlayClass } = this.state;
    const {
      data: { title, videoSource, showTitle, backgroundImage },
      height,
      isShowLogo = false
    } = this.props;
    return (
      <div>
        {showTitle ? <h4>{title}</h4> : null}

        <div className={videoPlayClass} style={{ height: `${height}px` }}>
          <div style={this.state.stylesFromParent} className="background-image-holder">
            <img src={backgroundImage} />
          </div>
          <div className="video-play-icon video-play-icon--sm" onClick={this.handleOnclick} />
          <video
            ref={this.videoRef}
            height={height}
            controlsList="nodownload"
            controls src={videoSource}>
            {videoSource ? <source src={videoSource} type="video/mp4" /> : null}
            Your browser does not support HTML5 video.
          </video>
          {isShowLogo ? (
            <div className={`${styles['logo-beloga']}`}>
              <img className={styles['logo-video']} alt="logo" src="/images/logo-big.png" />
            </div>
          ): null}
        </div>
      </div>
    );
  }
}
