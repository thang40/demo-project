import React, { Component } from 'react';

import axios from 'axios';
import styles from './video-uploader-modal.module.scss';

import { VideoPlayer } from '../video-player/video-player';
import { VideoUploader } from '../video-uploader/video-uploader';
import { Modal } from '../modal/modal';
import { Alert } from '../alert/alert';

interface VideoUploaderModalPropsType {
  toggleModal: any;
  isOpen: any;
  handleUploadVideo: any;
  progressUploadVideo: number;
  data: any;
  userInfo: any;
  uploadType: any;
}

export class UploadVideoWindow extends Component<VideoUploaderModalPropsType> {
  state = {
    percent: '0%',
    notify: '',
    video: undefined,
    preview: {
      title: 'Preview video',
      videoSource: '',
      showTitle: false,
      backgroundImage: '',
      imageCover: '',
      uploadType: '',
    }
  };

  selectVideoUpload = () => {};

  fileOptions = {
    multiple: false,
    component: 'div',
    action: this.selectVideoUpload(),
    headers: {},
    accept: 'video/*',
    onStart(file: any) {},
    onProgress: ({ percent }: any, file: any) => {},
    onSuccess: (ret: any) => {},
    onError(err: any) {},
    beforeUpload: (file: any, fileList: any) => {
      this.setState({
        notify: ''
      });
      URL.revokeObjectURL(this.state.preview.videoSource);
      if (file.type.split('/')[0] === 'video') {
        const video = document.createElement('video');
        video.src = window.URL.createObjectURL(file);
        video.preload = 'metadata';
        video.onloadedmetadata = () => {
          const duration = video.duration;
          if (duration > 40) {
            this.setState({
              notify: 'Video duration must be smaller than 40s.'
            });
          }
        };
        // Get back ground image if video is accept
        // let backGroundImage;
        // const captureImage = function() {
        //   const canvas = document.createElement('canvas');
        //   canvas.width = video.videoWidth;
        //   canvas.height = video.videoHeight;
        //   canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
        //   backGroundImage = canvas.toDataURL();
        // };
        this.setState({
          video: file,
          uploadType: this.props.uploadType,
          preview: {
            title: 'preview',
            videoSource: URL.createObjectURL(file),
            showTitle: false,
            backgroundImage: ''
          }
        });
        const { data } = this.props;
        return new Promise(resolve => {
          resolve(file);
        });
      }
      this.setState({
        notify: 'The upload must be video type.'
      });
      return false;
    },
    customRequest({ action, data, file, headers, onError, onProgress, onSuccess, withCredentials }: any) {}
  };

  handleOpenUploadVideoModal = () => {
    const { video, preview } = this.state;
    const { toggleModal, isOpen } = this.props;
    this.setState({
      video: isOpen ? undefined : video,
      preview: isOpen
        ? {
            title: 'Preview video',
            videoSource: '',
            showTitle: false,
            backgroundImage: '',
            imageCover: ''
          }
        : preview
    });
    toggleModal();
  };

  onSubmitVideo = () => {
    const { handleUploadVideo, uploadType } = this.props;
    const {video} = this.state;
    handleUploadVideo(video, uploadType);
  };

  render() {
    const { notify, preview, video } = this.state;
    const { isOpen, progressUploadVideo } = this.props;
    const percent = `${Math.round(progressUploadVideo).toFixed(2)}%`;
    const disableWhenUpload = progressUploadVideo > 0 && progressUploadVideo < 100 ? true : false;
    const messageUploadSuccess = progressUploadVideo === 100 ? true : false;
    return (
      <Modal isOpen={isOpen} toggleModal={this.handleOpenUploadVideoModal}>
        <div className={`${styles['upload-height']} container`}>
          <div className={`row justify-content-center ${styles['margin-bottom']}`}>
            <div className="col-md-12">
              <h5 className={styles['header-style']}>Select your video</h5>
              <VideoUploader video={video} fileOptions={this.fileOptions} disableModal={disableWhenUpload} />
              <a
                className={`btn btn--primary ${styles['btn-submit-video']} ${disableWhenUpload ? 'disabled' : null} ${
                  notify ? 'disabled' : null
                }`}
                onClick={this.onSubmitVideo}
              >
                Save
              </a>

              <div className="progress-horizontal progress-horizontal--sm">
                <div className="progress-horizontal__bar" data-value="20">
                  <div className="progress-horizontal__progress" style={{ width: percent }} />
                </div>
                <span className="progress-horizontal__label">Progress: {percent}</span>
                {messageUploadSuccess ? <Alert type="success" message="Upload video successfully!" /> : null}
                <p className="text-danger">{notify}</p>
              </div>
            </div>
            <div className="col-md-12">
              <h5>Preview Video</h5>
              <VideoPlayer isShowLogo={true} data={preview} height="auto" />
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}
