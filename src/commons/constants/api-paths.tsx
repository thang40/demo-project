export class ApiPaths {
  static readonly UPLOAD_AVATAR = '/profile/avatar/';
  static readonly UPLOAD_VIDEO = '/profile/cover-video/';
  static readonly UPLOAD_VIDEO_EDUCATION = '/profile/education-video/';
  static readonly UPLOAD_VIDEO_EXPERIENCE = '/profile/job-video/';
  static readonly CHANGE_EMAIL = '/users/email/change/';
  static readonly CHANGE_PASSWORD = '/auth/password/change/';
  static readonly CONTACT_US = '/contact/';
  static readonly ACTIVATE = '/auth/registration/verify-email/';
}
