export class RoutePaths {
  static readonly INDEX = '/';
  static readonly HOME = '/home';
  static readonly USER = '/user';
  static readonly USER_PROFILE = {
    PATH: '/user/:username',
    getPath: (username: string) => `/user/${username}`
  };
  static readonly USER_UPDATE_PROFILE = {
    PATH: '/user/:username/update-profile',
    getPath: (username: string) => `/user/${username}/update-profile`
  };
  static readonly USER_ACCOUNT_SETTING = {
    PATH: '/user/:username/account-setting',
    getPath: (username: string) => `/user/${username}/account-setting`
  };
  static readonly LOGIN = '/login';
  static readonly REGISTER = '/register';
  static readonly POLICY = '/privacy-policy';
  static readonly TERM_AND_CONDITION = '/terms-and-conditions';
  static readonly NOT_FOUND = '/404-not-found';
  static readonly CONTACT_US = '/contact-us';
  static readonly ACTIVATE_ACCOUNT = {
    PATH: '/activate-account/:key',
    getPath: (key: string) => `/activate-account/${key}`
  };
  static readonly ACTIVATE_LOGIN = {
    PATH: '/login/:key',
    getPath: (key: string) => `/login/${key}`
  };
  static readonly AFTER_REGISTER_SUCCESS = '/register-success';
  static readonly CHANGE_EMAIL_SUCCESS = '/change-email-success';
}
