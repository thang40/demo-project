export interface RegisterType {
  firstName: string;
  lastName: string;
  userName: string;
  emailAddress: string;
  password: string;
}
