import { combineReducers } from 'redux';
import { HomeReducer, HomeSaga } from './home.duck';
import { UserReducer, UserSaga } from './user.duck';
import { RegisterReducer, RegisterSaga } from './register.duck';
import { LoginReducer, LoginSaga } from './login.duck';
import { AuthReducer, AuthSaga } from './auth.duck';
import { UserEducationReducer, UserEducationSaga } from './user-education.duck';
import { UserExperienceReducer, UserExperienceSaga } from './user-experience.duck';
import { UserCertificateReducer, UserCertificationSaga } from './user-certification.duck';
import { LocationReducer, LocationSaga } from './location.duck';
import { ContactReducer, ContactSaga } from './contact.duck';
import { InterestReducer, InterestSaga } from './interest.duck';
import { UserPdfReducer, UserPdfSaga } from './user-pdf.duck';
import { all } from 'redux-saga/effects';

export const RootReducer = combineReducers({
  HomeReducer,
  UserReducer,
  UserEducationReducer,
  UserExperienceReducer,
  UserCertificateReducer,
  LoginReducer,
  RegisterReducer,
  AuthReducer,
  LocationReducer,
  ContactReducer,
  InterestReducer,
  UserPdfReducer
});

export function* rootSaga() {
  yield all([
    ...HomeSaga,
    ...UserSaga,
    ...UserEducationSaga,
    ...UserExperienceSaga,
    ...UserCertificationSaga,
    ...LoginSaga,
    ...RegisterSaga,
    ...AuthSaga,
    ...LocationSaga,
    ...ContactSaga,
    ...InterestSaga,
    ...UserPdfSaga
  ]);
}
