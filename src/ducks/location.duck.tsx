import { Reducer } from 'redux';
import { ActionBaseType } from '../commons/types';
import { put, takeLatest, call, take } from 'redux-saga/effects';
import { api } from '@demo/demo-ts-sdk';
import { handleCommonError } from '../services';
import { delay } from 'redux-saga';

export enum LocationActionTypes {
  LOCATION_FETCH_COUNTRY_REQUEST = '@@LOCATION/LOCATION_FETCH_COUNTRY_REQUEST',
  LOCATION_FETCH_COUNTRY_SUCCESS = '@@LOCATION/LOCATION_FETCH_COUNTRY_SUCCESS',
  LOCATION_FETCH_COUNTRY_ERROR = '@@LOCATION/LOCATION_FETCH_COUNTRY_ERROR',

  LOCATION_FETCH_STATE_REQUEST = '@@LOCATION/LOCATION_FETCH_STATE_REQUEST',
  LOCATION_FETCH_STATE_SUCCESS = '@@LOCATION/LOCATION_FETCH_STATE_SUCCESS',
  LOCATION_FETCH_STATE_ERROR = '@@LOCATION/LOCATION_FETCH_STATE_ERROR',

  LOCATION_FETCH_CITY_REQUEST = '@@LOCATION/LOCATION_FETCH_CITY_REQUEST',
  LOCATION_FETCH_CITY_SUCCESS = '@@LOCATION/LOCATION_FETCH_CITY_SUCCESS',
  LOCATION_FETCH_CITY_ERROR = '@@LOCATION/LOCATION_FETCH_CITY_ERROR'
}

// action creator
export const fetchCountryAction = () => ({
  type: LocationActionTypes.LOCATION_FETCH_COUNTRY_REQUEST
});

export const fetchStateAction = (countryId: number) => ({
  type: LocationActionTypes.LOCATION_FETCH_STATE_REQUEST,
  payload: countryId
});

export const fetchCityAction = (countryId: number, stateId: number) => ({
  type: LocationActionTypes.LOCATION_FETCH_CITY_REQUEST,
  payload: { countryId, stateId }
});

// reducer
interface LocationState {
  readonly countries: any[];
  readonly states: any[];
  readonly cities: any[];
}

const initialState: LocationState = {
  countries: [],
  states: [],
  cities: []
};

export const LocationReducer: Reducer<LocationState, ActionBaseType> = (state = initialState, action) => {
  switch (action.type) {
    case LocationActionTypes.LOCATION_FETCH_COUNTRY_SUCCESS: {
      return { ...state, countries: action.payload, states: [], cities: [] };
    }
    case LocationActionTypes.LOCATION_FETCH_STATE_SUCCESS: {
      return { ...state, states: action.payload, cities: [] };
    }
    case LocationActionTypes.LOCATION_FETCH_CITY_SUCCESS: {
      return { ...state, cities: action.payload };
    }
    default: {
      return state;
    }
  }
};

// selector
export const selectCountries = (state: any) => state.LocationReducer.countries;
export const selectStates = (state: any) => state.LocationReducer.states;
export const selectCities = (state: any) => state.LocationReducer.cities;

// side effect
function* watchFetchCountry(): any {
  yield call(delay, 100);
  try {
    const data = yield api.Location.countries(1, 100);
    debugger
    yield put({
      type: LocationActionTypes.LOCATION_FETCH_COUNTRY_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield handleCommonError(error.code);
  }
}

function* watchFetchState(action: ActionBaseType): any {
  try {
    yield call(delay, 100);
    const countryId = action.payload;
    const data =
      countryId !== -1 && countryId !== undefined ? yield api.Location.states(countryId, 1, 100) : { results: [] };
    debugger
      yield put({
      type: LocationActionTypes.LOCATION_FETCH_STATE_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield handleCommonError(error.code);
  }
}

function* watchFetchCity(action: ActionBaseType): any {
  try {
    yield call(delay, 100);
    const { countryId, stateId } = action.payload;
    const data =
      stateId !== -1 && stateId !== undefined && countryId !== -1 && countryId !== undefined
        ? yield api.Location.cities(countryId, stateId, 1, 100)
        : { results: [] };
    yield put({
      type: LocationActionTypes.LOCATION_FETCH_CITY_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield handleCommonError(error.code);
  }
}

export const LocationSaga = [
  takeLatest(LocationActionTypes.LOCATION_FETCH_COUNTRY_REQUEST, watchFetchCountry),
  takeLatest(LocationActionTypes.LOCATION_FETCH_STATE_REQUEST, watchFetchState),
  takeLatest(LocationActionTypes.LOCATION_FETCH_CITY_REQUEST, watchFetchCity)
];
