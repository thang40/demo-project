import React from 'react';

export const LoadableHomeScene = React.lazy(() => import('../home/home.scene'));

export const LoadableRegisterScene = React.lazy(() => import('../register/register.scene'));

export const LoadabeAfterRegisterScene = React.lazy(() => import('../register/afterregister.scene'));
export const LoadableUserScene = React.lazy(() => import('../user/user.scene'));

export const LoadableLoginScene = React.lazy(() => import('../login/login.scene'));

export const LoadablePrivacyPolicyScene = React.lazy(() => import('../privacy-policy/privacy-policy.scene'));

export const LoadableTermsConditionsScene = React.lazy(() =>
  import('../terms-and-conditions/terms-and-conditions.scene')
);

export const LoadableContactUsScene = React.lazy(() => import('../contact-us/contact-us.scene'));

export const LoadableNotFoundScene = React.lazy(() => import('../not-found/not-found.scene'));

export const LoadableActivateAccountScene = React.lazy(() => import('../activate-account/activate-account.scene'));
