import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  GenericList,
  JobOpeningItem,
  SearchBox,
  IconButton,
  VideoPlayer,
  VideoListItem,
  UserSidebar,
  SkillListItem,
  InterestListItem
} from '../../commons/components';
import { searchRequestAction, selectSearchResult } from '../../ducks/home.duck';
import { selectUserInfo } from '../../ducks/user.duck';
import { UserType } from '../../commons/types/view-model';
import { enhanceGenericList, enhanceUserSidebar } from '../../HOCs';
import styles from './home.scene.module.scss';
import { selectAuthState } from '../../ducks/auth.duck';
import { RoutePaths } from '../../commons/constants';

interface HomeSceneProps {
  actionCreator1: any;
  selectedData: any;
  searchRequestAction: any;
  searchData: any;
  getJobOpeningList: any;
  videoListItemData: any;
  userInfo?: any;
  logoutAction: any;
  uploadAvatarRequestAction: any;
  isAuthenticated: boolean;
  updateUserContactInfoAction: any;
  checkValidUsernameAction: any;
  history: any;
  locaiton: any;
}

const EnhancedUserSidebar = enhanceUserSidebar(UserSidebar);
const EnhancedSkillList = enhanceGenericList(GenericList, SkillListItem);
const EnhancedInterestList = enhanceGenericList(GenericList, InterestListItem);
const EnhancedJobOpeningList = enhanceGenericList(GenericList, JobOpeningItem);
const EnhancedVideoList = enhanceGenericList(GenericList, VideoListItem);

const sidebarSections = [
  <EnhancedSkillList
    title="Skills"
    WrapperComponent={(props: any) => {
      return <div className={styles['listitem-container']}>{props.children}</div>;
    }}
    itemClassName="col-xl-6 col-lg-12 col-md-6 col-6"
  />,
  <EnhancedInterestList
    title="Interests"
    WrapperComponent={(props: any) => {
      return <div className={styles['listitem-container']}>{props.children}</div>;
    }}
    itemClassName="col-xl-4 col-lg-6 col-md-4 col-4"
  />
];

class HomeScene extends Component<HomeSceneProps> {
  componentWillMount() {
    const { history, userInfo } = this.props;

    if (userInfo) {
      const validPathname = RoutePaths.USER_PROFILE.getPath(userInfo.username);
      history.push(validPathname);
    } else {
      history.push(RoutePaths.LOGIN);
    }
  }

  renderToolBox = () => (
    <div className={`container box-shadow ${styles['modified-container']}`}>
      <div className={`row ${styles['icon-buttons-wrapper']}`}>
        <div className="col-4">
          <IconButton iconClass="icon-Upload-2" buttonName="Input" />
        </div>
        <div className="col-4">
          <IconButton iconClass="icon-Paper-Plane" buttonName="Outbox" />
        </div>
        <div className="col-4">
          <IconButton iconClass="icon-Flag-4" buttonName="Updates" />
        </div>
      </div>
    </div>
  );

  render() {
    const { searchRequestAction, searchData, userInfo, isAuthenticated } = this.props;
    const userSidebarWrapperClass = userInfo ? 'demo-container' : 'demo-container hide';
    return (
      <div>
        <div className={`container ${styles['margin-top']}`}>
          <div className="row">
            {isAuthenticated ? (
              <div className={`col-lg-3 ${userSidebarWrapperClass}`}>
                <EnhancedUserSidebar sections={sidebarSections} userInfo={userInfo} />
              </div>
            ) : null}

            <div className={userInfo ? 'col-lg-6' : 'col-lg-8'}>
              <SearchBox data={searchData} actionSearch={searchRequestAction} />
              <div className={`container box-shadow ${styles['modified-container']}`}>
                <GenericList
                  hasIcon={false}
                  title="Video Resume"
                  data={[
                    {
                      title: 'Youtube video',
                      videoSource: 'http://techslides.com/demos/sample-videos/small.mp4',
                      showTitle: false,
                      backgroundImage: '/images/blog-1.jpg'
                    }
                  ]}
                  itemClassName="col-12"
                  listItemProps={{ height: '450' }}
                  ListItemComponent={VideoPlayer}
                />
                <EnhancedVideoList hasIcon={false} title="Other posts" itemClassName="col-12" />
              </div>
            </div>

            <div className={userInfo ? 'col-lg-3' : 'col-lg-4'}>
              {this.renderToolBox()}
              <div className={`container box-shadow ${styles['modified-container']}`}>
                <EnhancedJobOpeningList
                  hasIcon={false}
                  title="Job Opennings"
                  hasAddIcon={false}
                  itemClassName="col-12"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    searchData: selectSearchResult(state),
    userInfo: selectUserInfo(state),
    ...selectAuthState(state)
  };
};

export default connect(
  mapStateToProps,
  {
    searchRequestAction
  }
)(HomeScene);
