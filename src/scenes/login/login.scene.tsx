import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createForm } from 'rc-form';
import { Link } from 'react-router-dom';
import { selectIsNewUser, selectIsUserInfoExists, selectUserInfo } from '../../ducks/user.duck';
import { FormError, LoadingIcon, FacebookSocial, GoogleSocial, LinkedinSocial } from '../../commons/components';
import {
  loginAction,
  selectLoginState,
  facebookLoginAction,
  googleLoginAction,
  linkedinLoginAction,
  activateAction,
  selectActivateState
} from '../../ducks/login.duck';
import styles from './login.scene.module.scss';
import { RoutePaths } from '../../commons/constants';
import { UserType } from '../../commons/types/view-model';
import { selectCanAccessLogin, selectAuthState } from '../../ducks/auth.duck';
import { loadingWithConfig } from '../../HOCs';

const LoginForm = React.lazy(() => import('../../commons/components/_forms/login-form/login-form'));
const DefaultLoading = loadingWithConfig(LoadingIcon, 'white', 'component-loading-wrapper', 30);

interface LoginSceneProps {
  loginAction: any;
  isRedirect: boolean;
  history: any;
  isLoading: boolean;
  isNewUser: boolean;
  isUserInfoExists: boolean;
  errorFromServer: string;
  facebookLoginAction: any;
  googleLoginAction: any;
  linkedinLoginAction: any;
  userInfo: UserType;
  canAccessPage: boolean;
  isAuthenticated: boolean;
  isInvalidToken: boolean;
  activateAction: any;
  selectActivateState: any;
  match: any;
}

const enhanceLoginForm = createForm()(LoginForm);

class LoginScene extends Component<LoginSceneProps> {
  backgroundStyle = {
    background: `url('/images/login-background.jpg')`,
    opacity: 1
  };

  componentWillMount() {
    const { activateAction, match } = this.props;
    if (match.params.key) {
      activateAction(match.params);
    } else {
      const { history, isAuthenticated, isNewUser, isUserInfoExists, userInfo } = this.props;
      if (isAuthenticated) {
        if (isUserInfoExists) {
          isNewUser
            ? history.push(RoutePaths.USER_UPDATE_PROFILE.getPath(userInfo.username))
            : history.push(RoutePaths.USER_PROFILE.getPath(userInfo.username));
        }
      }
    }
  }

  componentDidMount() {
    const { history, isAuthenticated, isNewUser, isUserInfoExists, userInfo } = this.props;
    if (isAuthenticated) {
      if (isUserInfoExists) {
        isNewUser
          ? history.push(RoutePaths.USER_UPDATE_PROFILE.getPath(userInfo.username))
          : history.push(RoutePaths.USER_PROFILE.getPath(userInfo.username));
      }
    }
  }

  componentWillUpdate(nextProps: any) {
    const { history, isNewUser, isUserInfoExists, userInfo } = nextProps;

    if (isUserInfoExists) {
      isNewUser
        ? history.push(RoutePaths.USER_UPDATE_PROFILE.getPath(userInfo.username))
        : history.push(RoutePaths.USER_PROFILE.getPath(userInfo.username));
    }
  }

  render() {
    const EnhanceLoginForm = enhanceLoginForm;
    const {
      loginAction,
      isLoading,
      errorFromServer,
      facebookLoginAction,
      googleLoginAction,
      linkedinLoginAction,
      isAuthenticated
    } = this.props;
    return !isAuthenticated ? (
      <section className="text-center height-110 imagebg" data-overlay="5">
        <div className="background-image-holder" style={{ ...this.backgroundStyle }} />
        <div className="container">
          <div className="row">
            <div className="col-md-7 col-lg-5">
              <h2 className={styles['margin-top']}>Login to continue</h2>
              <FacebookSocial handleLoginAction={facebookLoginAction} buttonTitle="Login with Facebook" />
              <GoogleSocial handleLoginAction={googleLoginAction} buttonTitle="Login with Google" />
              <LinkedinSocial handleLoginAction={linkedinLoginAction} buttonTitle="Login with LinkedIn" />
              <hr />
              <p className="lead">Sign in or create an account</p>
              {errorFromServer ? (
                <div className={styles['form-error']}>
                  <FormError text={errorFromServer} />
                </div>
              ) : null}
              <React.Suspense fallback={DefaultLoading}>
                <EnhanceLoginForm handleSubmitAction={loginAction} isLoading={isLoading} />
              </React.Suspense>

              <span className="type--fine-print block">
                Dont have an account yet? <Link to="/register">Create account</Link>
              </span>
              <span className="type--fine-print block">
                Forgot your email or password? <Link to="/">Recover account</Link>
              </span>
            </div>
          </div>
        </div>
      </section>
    ) : (
      <React.Fragment />
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    ...selectLoginState(state),
    isNewUser: selectIsNewUser(state),
    isUserInfoExists: selectIsUserInfoExists(state),
    userInfo: selectUserInfo(state),
    ...selectAuthState(state),
    activateState: selectActivateState(state)
  };
};

export default connect(
  mapStateToProps,
  { loginAction, facebookLoginAction, googleLoginAction, linkedinLoginAction, activateAction }
)(LoginScene);
