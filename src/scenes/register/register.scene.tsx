import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createForm } from 'rc-form';
import {
  submitRegisterUserAction,
  checkValidEmailAction,
  checkValidUsernameAction,
  selectRegisterState
} from '../../ducks/register.duck';

import { facebookLoginAction, googleLoginAction, linkedinLoginAction } from '../../ducks/login.duck';

import { FacebookSocial, GoogleSocial, LoadingIcon, LinkedinSocial } from '../../commons/components';
import styles from './register.scene.module.scss';
import { RoutePaths } from '../../commons/constants';
import { loadingWithConfig } from '../../HOCs';

export const RegisterForm = React.lazy(() => import('../../commons/components/_forms/register-form/register-form'));

const DefaultLoading = loadingWithConfig(LoadingIcon, 'white', 'component-loading-wrapper', 30);

interface RegisterPropsType {
  submitRegisterUserAction: any;
  form: any;
  isRedirect: boolean;
  history: any;
  isLoading: boolean;
  errorFromServer: string;
  checkValidEmailAction: any;
  checkValidUsernameAction: any;
  facebookLoginAction: any;
  googleLoginAction: any;
  linkedinLoginAction: any;
}

class RegisterComponent extends Component<RegisterPropsType> {
  formRef: any;

  componentDidUpdate() {
    const { isRedirect, history } = this.props;
    if (isRedirect) {
      history.push(RoutePaths.AFTER_REGISTER_SUCCESS);
    }
  }

  enhanceRegisterForm = createForm()(RegisterForm);

  backgroundStyle = {
    background: `url('/images/login-background.jpg')`,
    opacity: 1
  };

  render() {
    const {
      submitRegisterUserAction,
      isLoading,
      errorFromServer,
      checkValidEmailAction,
      checkValidUsernameAction,
      facebookLoginAction,
      googleLoginAction,
      linkedinLoginAction
    } = this.props;

    const EnhanceRegisterForm = this.enhanceRegisterForm;
    return (
      <section className="imagebg" data-overlay="5">
        <div className="background-image-holder" style={{ ...this.backgroundStyle }} />
        <div className="container">
          <div className="row">
            <div className="w-100 d-flex justify-content-center">
              <div className="col-md-12 col-lg-12">
                <h2 className="text-center">Join demo</h2>
                <br />
              </div>
            </div>
            <div className="w-100 d-flex justify-content-center">
              <div className="col-md-6 col-lg-6">
                <FacebookSocial handleLoginAction={facebookLoginAction} buttonTitle="Sign up with Facebook" />
                <GoogleSocial handleLoginAction={googleLoginAction} buttonTitle="Sign up with Google" />
                <LinkedinSocial handleLoginAction={linkedinLoginAction} buttonTitle="Sign up with LinkedIn" />
                <hr className={styles['separate-hr']} data-title="OR" />
              </div>
            </div>
            <React.Suspense fallback={DefaultLoading}>
              <EnhanceRegisterForm
                isLoading={isLoading}
                actionSubmit={submitRegisterUserAction}
                errorFromServer={errorFromServer}
                wrappedComponentRef={(inst: any) => (this.formRef = inst)}
                actionCheckEmail={checkValidEmailAction}
                actionCheckUsername={checkValidUsernameAction}
              />
            </React.Suspense>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    ...selectRegisterState(state)
  };
};

export default connect(
  mapStateToProps,
  {
    submitRegisterUserAction,
    checkValidEmailAction,
    checkValidUsernameAction,
    facebookLoginAction,
    googleLoginAction,
    linkedinLoginAction
  }
)(RegisterComponent);
