import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GenericList, SkillListItem, InterestListItem, UserSidebar, LoadingIcon } from '../../../commons/components';
import {
  updateUserContactInfoAction,
  uploadAvatarRequestAction,
  updateUserEmailAction,
  updateUserPasswordAction,
  selectErrorChangePassword,
  selectIsRedirect,
  selectIsChangeEmailSuccess,
  selectErrorFromServer,
  selectUserInfo,
  getUserInfoAction
} from '../../../ducks/user.duck';
import { checkValidEmailAction, checkValidUsernameAction } from '../../../ducks/register.duck';
import { createForm } from 'rc-form';
import styles from './account-setting.scene.module.scss';
import { RoutePaths } from '../../../commons/constants';
import { UserType } from '../../../commons/types/view-model';
import { enhanceGenericList, enhanceUserSidebar, loadingWithConfig } from '../../../HOCs';
import { updateUserMapper } from '../../../utils/mapper';

export const PersonalForm = React.lazy(() => import('../../../commons/components/_forms/personal-form/personal-form'));
export const ChangePasswordForm = React.lazy(() =>
  import('../../../commons/components/_forms/change-password-form/change-password-form')
);

const DefaultLoading = loadingWithConfig(LoadingIcon, 'white', 'component-loading-wrapper', 30);

interface AccountSettingComponentProps {
  history: any;
  updateUserContactInfoAction: any;
  updateUserEmailAction: any;
  updateUserPasswordAction: any;
  getErrorChangePassword: any;
  errorChangePassword: any;
  isRedirect: boolean;
  isLoading: boolean;
  isChangeEmailSuccess: boolean;
  uploadAvatarRequestAction: any;
  userInfo?: UserType;
  getUserInfoAction: any;
  checkValidEmailAction: any;
  checkValidUsernameAction: any;
  errorFromServer: string;
}

const EnhanceUserSidebar = enhanceUserSidebar(UserSidebar);
const EnhancedSkillList = enhanceGenericList(GenericList, SkillListItem);
const EnhancedInterestList = enhanceGenericList(GenericList, InterestListItem);

const sidebarSections = [
  <EnhancedSkillList
    title="Skills"
    itemClassName="col-6"
    WrapperComponent={(props: any) => {
      return <div className={styles['listitem-container']}>{props.children}</div>;
    }}
  />,
  <EnhancedInterestList
    title="Interests"
    itemClassName="col-4"
    WrapperComponent={(props: any) => {
      return <div className={styles['listitem-container']}>{props.children}</div>;
    }}
  />
];

class AccountSettingScene extends Component<AccountSettingComponentProps> {
  constructor(props: any) {
    super(props);
  }

  state = {
    currentStep: 1,
    isInit: false,
    containerClass: 'demo-container hide'
  };
  totalStep = 2;
  formRefPersonal: any;
  formRefPassword: any;

  componentWillMount() {
    const { getUserInfoAction } = this.props;
    getUserInfoAction();
  }

  componentWillUpdate(nextProps: any) {
    const { history, location, userInfo, isChangeEmailSuccess } = nextProps;
    if (isChangeEmailSuccess) {
      history.push(RoutePaths.CHANGE_EMAIL_SUCCESS);
    } else {
      const pathname = location.pathname;
      const validPathname = RoutePaths.USER_ACCOUNT_SETTING.getPath(userInfo.username);
      if (userInfo && pathname !== validPathname) {
        history.replace(RoutePaths.NOT_FOUND);
      }
      this.formRefPersonal.props.form.setFieldsValue({ ...updateUserMapper.apiToForm(userInfo) });
    }
  }

  enhancePersonalForm = createForm()(PersonalForm);
  enhanceChangePasswordForm = createForm()(ChangePasswordForm);

  render() {
    const {
      userInfo,
      updateUserContactInfoAction,
      updateUserEmailAction,
      updateUserPasswordAction,
      errorChangePassword,
      isLoading,
      isChangeEmailSuccess,
      errorFromServer,
      checkValidEmailAction,
      checkValidUsernameAction
    } = this.props;
    const EnhancePersonalForm = this.enhancePersonalForm;
    const EnhanceChangePasswordForm = this.enhanceChangePasswordForm;
    return (
      <div>
        <div className={`container ${styles['account-setting-form']}`}>
          <div className="row">
            <div className={`col-lg-4 `}>
              <EnhanceUserSidebar sections={sidebarSections} userInfo={userInfo} />
            </div>

            <div className="col-lg-8">
              <div className={`container box-shadow ${styles['modified-container']}`}>
                <React.Suspense fallback={DefaultLoading}>
                  <h3>Personal Information</h3>
                  <EnhancePersonalForm
                    userInfo={userInfo}
                    isLoading={isLoading}
                    actionSubmit={updateUserContactInfoAction}
                    actionSubmitEmail={updateUserEmailAction}
                    errorFromServer={errorFromServer}
                    wrappedComponentRef={(inst: any) => (this.formRefPersonal = inst)}
                    actionCheckEmail={checkValidEmailAction}
                    checkValidUsernameAction={checkValidUsernameAction}
                  />
                  <h3 className={styles['password-title-group']}>Change Password</h3>
                  <EnhanceChangePasswordForm
                    userInfo={userInfo}
                    isLoading={isLoading}
                    actionSubmit={updateUserPasswordAction}
                    errorFromServer={errorFromServer}
                    wrappedComponentRef={(inst: any) => (this.formRefPassword = inst)}
                    errorChangePassword={errorChangePassword}
                  />
                </React.Suspense>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    isRedirect: selectIsRedirect(state),
    errorFromServer: selectErrorFromServer(state),
    userInfo: selectUserInfo(state),
    errorChangePassword: selectErrorChangePassword(state),
    isChangeEmailSuccess: selectIsChangeEmailSuccess(state)
  };
};

export default connect(
  mapStateToProps,
  {
    updateUserContactInfoAction,
    updateUserEmailAction,
    uploadAvatarRequestAction,
    updateUserPasswordAction,
    getUserInfoAction,
    checkValidEmailAction,
    checkValidUsernameAction
  }
)(AccountSettingScene);
