import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  GenericList,
  EducationListItem,
  CertificationListItem,
  SkillListItem,
  InterestListItem,
  ExperienceListItem,
  IconButton,
  VideoPlayer,
  UserSidebar,
  UploadVideoWindow,
  LoadingIcon,
  Modal
} from '../../../commons/components';

import {
  uploadAvatarRequestAction,
  selectIsNewUser,
  selectIsUserInfoExists,
  selectUserInfo,
  getUserInfoAction,
  selectIsRedirect,
  uploadVideoRequestAction,
  uploadVideoResetAction,
  getProgressUploadVideo
} from '../../../ducks/user.duck';
import { exportProfilePdfAction, selectProfilePdfUrl } from '../../../ducks/user-pdf.duck';
import { logoutAction } from '../../../ducks/login.duck';
import { UserType } from '../../../commons/types/view-model';
import { enhanceGenericList, loadingWithConfig, enhanceUserSidebar } from '../../../HOCs';
import sassVariable from '../../../styles/variables.module.scss';
import styles from './profile.scene.module.scss';
import { RoutePaths } from '../../../commons/constants';
import { userInfo } from 'os';

interface ProfileSceneProps {
  selectedData: any;
  searchRequestAction: any;
  searchData: any;
  getJobOpeningList: any;
  getUserInfoAction: any;
  videoListItemData: any;
  userInfo?: any;
  logoutAction: any;
  uploadAvatarRequestAction: any;
  history: any;
  updateUserInfoAction: any;
  checkValidUsernameAction: any;
  updateUserContactInfoAction: any;
  uploadVideoRequestAction: any;
  uploadVideoResetAction: any;
  progressUploadVideo: number;
  exportProfilePdfAction: any;
  profilePdfUrl: any;
}

const EnhanceUserSidebar = enhanceUserSidebar(UserSidebar);
const EnhancedEducationList = enhanceGenericList(GenericList, EducationListItem);
const EnhancedExperienceList = enhanceGenericList(GenericList, ExperienceListItem);
const EnhancedCertificationList = enhanceGenericList(GenericList, CertificationListItem);
const EnhancedSkillList = enhanceGenericList(GenericList, SkillListItem);
const EnhancedInterestList = enhanceGenericList(GenericList, InterestListItem);

const DefaultLoading = loadingWithConfig(LoadingIcon, sassVariable.mainColor, 'component-loading-wrapper', 30);

const sidebarSections = [
  <EnhancedSkillList
    title="Skills"
    itemClassName="col-6"
    WrapperComponent={(props: any) => {
      return <div className={styles['listitem-container']}>{props.children}</div>;
    }}
  />,
  <EnhancedInterestList
    title="Interests"
    itemClassName="col-4"
    WrapperComponent={(props: any) => {
      return <div className={styles['listitem-container']}>{props.children}</div>;
    }}
  />
];

class ProfileScene extends Component<ProfileSceneProps> {
  state = {
    isOpenUploadModal: false,
    isOpenUpContactInfoModal: false,
    uploadType: 'resume',
    showViewVideo: false,
    dataPreview: {
      title: 'View video',
      videoSource: '',
      showTitle: false,
      backgroundImage: '',
      imageCover: '',
      uploadType: ''
    }
  };

  componentWillMount() {
    const { getUserInfoAction } = this.props;
    getUserInfoAction();
  }

  componentWillUpdate(nextProps: any) {
    const { history, location, userInfo, isRedirect, profilePdfUrl } = nextProps;
    const pathname = location.pathname;
    const validPathname = RoutePaths.USER_PROFILE.getPath(userInfo.username);
    if (userInfo && pathname !== validPathname) {
      isRedirect ? history.replace(validPathname) : history.replace(RoutePaths.NOT_FOUND);
    }
    if (profilePdfUrl.pdf_url) {
      window.open(profilePdfUrl.pdf_url, '_blank');
    }
  }

  handleOpenUploadVideoModal = () => {
    const { uploadVideoResetAction } = this.props;
    uploadVideoResetAction();
    this.setState({
      uploadType: 'resume',
      isOpenUploadModal: !this.state.isOpenUploadModal
    });
  };

  handleOpenUploadVideoModalEducation = () => {
    const { uploadVideoResetAction } = this.props;
    uploadVideoResetAction();
    this.setState({
      uploadType: 'education',
      isOpenUploadModal: !this.state.isOpenUploadModal
    });
  };

  handleOpenUploadVideoModalExperience = () => {
    const { uploadVideoResetAction } = this.props;
    uploadVideoResetAction();
    this.setState({
      uploadType: 'experience',
      isOpenUploadModal: !this.state.isOpenUploadModal
    });
  };

  handleExportPdf = (e: any) => {
    e.preventDefault();
    const { userInfo, exportProfilePdfAction } = this.props;
    exportProfilePdfAction(userInfo.user_id);
  };

  renderToolBox = () => (
    <div className={`container ${styles['menu-container']}`}>
      <div className={`row ${styles['icon-buttons-wrapper']}`}>
        <a className="col-6">
          <IconButton className="justify-content-end" iconClass="icon-Upload-2" buttonName="Share on Social" />
        </a>
        <a className="col-6" onClick={this.handleExportPdf}>
          <IconButton iconClass="icon-Paper-Plane" className="justify-content-end" buttonName="Export Profile to PDF" />
        </a>
      </div>
    </div>
  );

  toggleViewVideo = (type: string) => {
    const { userInfo } = this.props;
    if (type) {
      if (type == 'Experience') {
        this.setState({
          showViewVideo: !this.state.showViewVideo,
          dataPreview: {
            title: 'View Video',
            videoSource: userInfo && userInfo.job_video_url ? userInfo.job_video_url : '/videos/video_introduction.mp4',
            showTitle: false,
            backgroundImage:
              userInfo && userInfo.job_cover_image_url
                ? userInfo.job_cover_image_url
                : '/images/image_cover_introduction.png'
          }
        });
      } else if (type == 'Education') {
        this.setState({
          showViewVideo: !this.state.showViewVideo,
          dataPreview: {
            title: 'View Video',
            videoSource:
              userInfo && userInfo.school_video_url ? userInfo.school_video_url : '/videos/video_introduction.mp4',
            showTitle: false,
            backgroundImage:
              userInfo && userInfo.school_cover_image_url
                ? userInfo.school_cover_image_url
                : '/images/image_cover_introduction.png'
          }
        });
      }
    } else {
      this.setState({ showViewVideo: !this.state.showViewVideo });
    }
  };

  render() {
    const { userInfo, uploadVideoRequestAction, progressUploadVideo } = this.props;
    const { isOpenUploadModal, uploadType, showViewVideo } = this.state;
    const userSidebarContainerClass = userInfo ? 'demo-container' : 'demo-container hide';
    let videoUrl = null;
    if (uploadType == 'resume') {
      videoUrl = userInfo && userInfo.video_url ? userInfo.video_url : '';
    } else if (uploadType == 'experience') {
      videoUrl = userInfo && userInfo.job_video_url ? userInfo.job_video_url : '';
    } else if (uploadType == 'education') {
      videoUrl = userInfo && userInfo.school_video_url ? userInfo.school_video_url : '';
    }
    return (
      <div>
        <div className={`container ${styles['padding-top']}`}>
          <Modal isOpen={showViewVideo} toggleModal={this.toggleViewVideo}>
            <VideoPlayer isShowLogo={true} data={this.state.dataPreview} height="auto" />
          </Modal>
          <UploadVideoWindow
            toggleModal={this.handleOpenUploadVideoModal}
            isOpen={isOpenUploadModal}
            uploadType={uploadType}
            handleUploadVideo={uploadVideoRequestAction}
            progressUploadVideo={progressUploadVideo}
            userInfo={userInfo}
            data={{
              title: '',
              videoSource: videoUrl,
              showTitle: false,
              backgroundImage: ''
            }}
          />
          <div className="row">
            <div className="col-xl-5 offset-xl-7 col-lg-5 offset-lg-7 col-md-12 col-sm-12 col-12">
              {this.renderToolBox()}
            </div>
          </div>
          <div className="row">
            <div className={`col-12 col-md-12 col-lg-4 ${userSidebarContainerClass}`}>
              <EnhanceUserSidebar sections={sidebarSections} userInfo={userInfo} />
            </div>
            <div className="col-12 col-md-12 col-lg-8">
              <div className={`container box-shadow ${styles['modified-container']}`}>
                <GenericList
                  title="Video Resume"
                  iconClass="far fa-edit"
                  data={
                    userInfo && userInfo.video_url
                      ? [
                          {
                            title: '',
                            videoSource: userInfo && userInfo.video_url,
                            showTitle: false,
                            backgroundImage: userInfo && userInfo.cover_image_url
                          }
                        ]
                      : [
                          {
                            title: '',
                            videoSource: '/videos/video_introduction.mp4',
                            showTitle: false,
                            backgroundImage: '/images/image_cover_introduction.png'
                          }
                        ]
                  }
                  itemClassName="col-12"
                  listItemProps={{ height: '450', isShowLogo: true }}
                  ListItemComponent={VideoPlayer}
                  onIconClick={this.handleOpenUploadVideoModal}
                />
                <hr />
                <EnhancedExperienceList
                  title="Experience"
                  itemClassName="col-12"
                  iconText="Add"
                  viewVideo={this.toggleViewVideo}
                  isShowUploadButton={true}
                  isShowButtonVideo={userInfo && userInfo.job_video_url ? true : false}
                  onPressUploadVideo={this.handleOpenUploadVideoModalExperience}
                  LoadingComponent={DefaultLoading}
                />
                <hr />
                {/* <GenericList
                  title={'Video Experience'}
                  iconClass="far fa-edit"
                  isShowTitle={false}
                  data={
                    userInfo && userInfo.job_video_url
                      ? [
                          {
                            title: '',
                            videoSource: userInfo && userInfo.job_video_url,
                            showTitle: false,
                            backgroundImage: userInfo && userInfo.job_cover_image_url
                          }
                        ]
                      : []
                  }
                  itemClassName="col-12"
                  listItemProps={{ height: '450', isShowLogo: true }}
                  ListItemComponent={VideoPlayer}
                  onIconClick={this.handleOpenUploadVideoModalExperience}
                />
                <hr /> */}
                <EnhancedEducationList
                  title="Education"
                  viewVideo={this.toggleViewVideo}
                  isShowUploadButton={true}
                  isShowButtonVideo={userInfo && userInfo.school_video_url ? true : false}
                  onPressUploadVideo={this.handleOpenUploadVideoModalEducation}
                  itemClassName="col-12"
                  iconText="Add"
                  LoadingComponent={DefaultLoading}
                />
                <hr />
                {/* <GenericList
                  title={'Video Education'}
                  isShowTitle={false}
                  iconClass="far fa-edit"
                  data={
                    userInfo && userInfo.school_video_url
                      ? [
                          {
                            title: '',
                            videoSource: userInfo && userInfo.school_video_url,
                            showTitle: false,
                            backgroundImage: userInfo && userInfo.school_cover_image_url
                          }
                        ]
                      : []
                  }
                  itemClassName="col-12"
                  listItemProps={{ height: '450', isShowLogo: true }}
                  ListItemComponent={VideoPlayer}
                  onIconClick={this.handleOpenUploadVideoModalEducation}
                />
                <hr /> */}
                <EnhancedCertificationList
                  title="Awards & Certifications"
                  itemClassName="col-12"
                  iconText="Add"
                  LoadingComponent={DefaultLoading}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    isNewUser: selectIsNewUser(state),
    isUserInfoExists: selectIsUserInfoExists(state),
    userInfo: selectUserInfo(state),
    isRedirect: selectIsRedirect(state),
    progressUploadVideo: getProgressUploadVideo(state),
    profilePdfUrl: selectProfilePdfUrl(state)
  };
};

export default connect(
  mapStateToProps,
  {
    logoutAction,
    uploadAvatarRequestAction,
    uploadVideoRequestAction,
    uploadVideoResetAction,
    getUserInfoAction,
    exportProfilePdfAction
  }
)(ProfileScene);
