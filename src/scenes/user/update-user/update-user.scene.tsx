import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Steps, Step, FormError } from '../../../commons/components';
import {
  updateUserInfoAction,
  uploadAvatarRequestAction,
  selectIsRedirect,
  selectErrorFromServer,
  selectUserInfo,
  getUserInfoAction
} from '../../../ducks/user.duck';
import { createForm } from 'rc-form';
import styles from './update-user.scene.module.scss';
import { RoutePaths } from '../../../commons/constants';
import { updateUserMapper } from '../../../utils/mapper';

const InformationForm = React.lazy(() =>
  import('../../../commons/components/_forms/information-form/information-form')
);

const SocialConnectForm = React.lazy(() =>
  import('../../../commons/components/_forms/social-connect-form/social-connect-form')
);

interface UpdateUserComponentProps {
  history: any;
  updateUserInfoAction: any;
  isRedirect: boolean;
  uploadAvatarRequestAction: any;
  userInfo: any;
  getUserInfoAction: any;
  errorFromServer: string;
}

class UpdateUserScene extends Component<UpdateUserComponentProps> {
  constructor(props: any) {
    super(props);
  }

  state = {
    currentStep: 1,
    isInit: false,
    containerClass: 'demo-container hide'
  };
  totalStep = 2;
  formRefs: any = [];
  listSocial: any = {};
  enhancedInformationForm: any = createForm()(InformationForm);
  enhancedSocialConnectForm: any = createForm()(SocialConnectForm);

  sections: any = [this.enhancedInformationForm, this.enhancedSocialConnectForm];

  componentWillMount() {
    const { getUserInfoAction } = this.props;
    getUserInfoAction();
  }

  componentWillUpdate(nextProps: any) {
    const { history, location, userInfo, isRedirect } = nextProps;
    const pathname = location.pathname;
    const validPathname = RoutePaths.USER_UPDATE_PROFILE.getPath(userInfo.username);
    if (userInfo && pathname !== validPathname) {
      history.replace(RoutePaths.NOT_FOUND);
    }
    if (isRedirect) {
      history.push(RoutePaths.INDEX);
    }
    const { isInit } = this.state;
    if (userInfo && !isInit) {
      const mappedData = updateUserMapper.apiToForm(userInfo);
      this.formRefs.map((form: any) => {
        form.props.form.setFieldsValue({
          ...mappedData
        });
      });
      this.setState({
        isInit: true,
        containerClass: 'demo-container'
      });
    }
  }
  setLinkSocial (data: Object){
    this.listSocial = data;
  }
  submitData = () => {
    const { updateUserInfoAction } = this.props;
    let data = {};
    this.formRefs.map((form: any) => (data = { ...form.props.form.getFieldsValue(), ...data }));
    const mappedData = updateUserMapper.formToApi(data);
    mappedData.facebook = this.listSocial.facebook;
    mappedData.linkedin = this.listSocial.linkedin;
    mappedData.twitter = this.listSocial.twitter;
    mappedData.instagram = this.listSocial.instagram;
    debugger
    updateUserInfoAction(mappedData);
  };

  moveStep = (step: number) => {
    const { currentStep } = this.state;
    const { validateFields } = this.formRefs[currentStep - 1].props.form;
    validateFields((error: any, value: any) => {
      if (!error) {
        this.setState({
          currentStep: step
        });
      }
    });
  };

  renderStepButton = (text: string, step: number, handleClick: any = this.moveStep) => {
    return (
      <a className="btn btn--primary no-margin-bottom" onClick={() => handleClick(step)}>
        <span className="btn__text text-uppercase">{text}</span>
      </a>
    );
  };

  renderCaseStepButtons = () => {
    const { currentStep } = this.state;
    if (currentStep === 1) {
      return this.renderStepButton('Next', currentStep + 1);
    }
    if (currentStep === this.totalStep) {
      return (
        <React.Fragment>
          {this.renderStepButton('Previous', currentStep - 1)}
          {this.renderStepButton('Finish', 0, this.submitData)}
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        {this.renderStepButton('Previous', currentStep - 1)}
        {this.renderStepButton('Next', currentStep + 1)}
      </React.Fragment>
    );
  };

  renderStepContent = () => {
    const { currentStep, containerClass } = this.state;
    const { uploadAvatarRequestAction,userInfo } = this.props;
    return this.sections.map((section: any, index: any) => {
      const SectionComponent = section;
      const sectionWrapperClass = index !== currentStep - 1 ? 'display-none' : '';
      return (
        <div key={index} className={`${sectionWrapperClass} ${containerClass}`}>
          <SectionComponent
            setLinkSocial={this.setLinkSocial.bind(this)}
            userInfo={userInfo}
            handleUploadAvatar={uploadAvatarRequestAction}
            wrappedComponentRef={(inst: any) => (this.formRefs[index] = inst)}
          />
        </div>
      );
    });
  };

  render() {
    const { errorFromServer } = this.props;
    return (
      <div>
        <div className={`container ${styles['margin-top']}`}>
          <div className="row">
            <div className="col-12">
              <Steps current={this.state.currentStep}>
                <Step title="1. Personal Information" />
                <Step title="2. Connect Your Socials" />
              </Steps>
              <div className={styles['step-content']}>{this.renderStepContent()}</div>
              <div className="text-center">
                <FormError text={errorFromServer} />
              </div>
              <div className={styles['step-button']}>{this.renderCaseStepButtons()}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    isRedirect: selectIsRedirect(state),
    errorFromServer: selectErrorFromServer(state),
    userInfo: selectUserInfo(state)
  };
};

export default connect(
  mapStateToProps,
  {
    updateUserInfoAction,
    uploadAvatarRequestAction,
    getUserInfoAction
  }
)(UpdateUserScene);
