import { MONTHS } from '../../commons/constants';

export const apiToForm = (data: any) => {
  const formData = {
    ...data
    // places: {
    //   selectedCityId: data.city,
    //   selectedCountryId: data.country,
    //   selectedStateId: data.state
    // }
  };
  return formData;
};

export const formToApi = (formData: any) => {
  const apiData = {
    ...formData,
    user: {
      first_name: formData.first_name,
      last_name: formData.last_name,
      email: formData.email,
      username: formData.username
    }
    // country: formData.places.selectedCountryId,
    // state: formData.places.selectedStateId,
    // city: formData.places.selectedCityId
  };
  return apiData;
};
